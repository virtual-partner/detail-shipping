$(document).ready(function(){

	$('#mmenu').mmenu();
	
	$('#bannerslider').owlCarousel({
		loop:true,		
		margin:0,
		autoplay:true,
		autoplayTimeout:2000,
		autoplayHoverPause:true,
		nav:true,
		navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:1
			},
			1000:{
				items:1
			},
			1200:{
				items:1
			}
		}
	});

});

