<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //  Inserting admin data when seeded
        Admin::insert([
            'name'      =>  'Super Admin',
            'email'     =>  'admin@tcpl.com',
            'role'      =>  1,
            'password'  =>  bcrypt('Admin@1234!'),
            'created_at'=>  date('Y-m-d'),
            'updated_at'=>  date('Y-m-d')
        ]);
    }
}
