<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('item_name');
            $table->string('invoice_number');
            $table->text('shipping_address');
            $table->string('address_from')->nullable()->comment('This address is from where the item has been picked up.');
            $table->boolean('payment_status')->default(0)->comment('0 => Pending, 1 => Paid');
            $table->decimal('payment_amount',10,2)->nullable();
            $table->string('item_tracking_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
