<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubAdminFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = 0;
        if ($this->request->has('admin_id')) {

            $id = decrypt($this->request->get('admin_id'));
        }
        return [
            'action'    =>  'required|in:add,edit',
            'name'      =>  'required',
            'email'     =>  'required|email|unique:admins,email,'.$id,
            'password'  =>  'required_if:action,add|nullable|string'
        ];
    }
}
