<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactQueryRequest;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        //  Getting order data
        $orderData  =   \App\Models\Order::where('item_tracking_number',trim($request->tracking_number))->first();
        
        $orderData->order_status    =   $orderData->order_status->map(function($order_status){

                                            switch ($order_status->order_status) {
                                                case 'PICKED UP':
                                                    $order_status->color    =   'gray';
                                                    break;

                                                case 'DISPATCHED':
                                                    $order_status->color    =   'blue';
                                                    break;
                                                
                                                case 'IN TRANSIT':
                                                    $order_status->color    =   'yellow';
                                                    break;

                                                case 'RECEIVED IN HUB':
                                                    $order_status->color    =   'yellow';
                                                    break;
                                                
                                                case 'OUT FOR DELIVERY':
                                                    $order_status->color    =   '#40c340';
                                                    break;
                                                
                                                case 'DELIVERED':
                                                    $order_status->color    =   '#40c340';
                                                    break;
                                                
                                                case 'ON HOLD':
                                                    $order_status->color    =   'red';
                                                    break;
                                                
                                                default:
                                                    $order_status->color    =   '#0066ff87';
                                                    break;
                                            }
                                            return $order_status;
                                        });

        //  Returning view with order and order status data
        return view('order_tracking_details',compact(['orderData']));
    }


    public function saveContactQuery(ContactQueryRequest $request)
    {
        try {
            //  Saving contact query of user
            \App\Models\ContactQuery::insert([
                'name'      =>  trim($request->name),
                'email'     =>  trim($request->email),
                'query'     =>  trim($request->contact_query),
                'created_at'=>  date('Y-m-d H:i:s'),
                'updated_at'=>  date('Y-m-d H:i:s')
            ]);

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Query has been submitted successfully.']);

        } catch (\Exception $e) {
            
            //  Returning server error response
            return response()->json(['status'=>false,'message'=>'Server Error! Please try again later after some time.']);
        }
    }


    public function blogDetails($blogSlug=null)
    {
        //  If there is no slug then i.e. blog page is being clicked then
        if ($blogSlug) {
            //  Getting details of blog
            $blogDetails    =   \App\Models\Blog::where('slug',$blogSlug)->first();
        } else {
            //  Getting latest blog details
            $blogDetails    =   \App\Models\Blog::latest()->first();
        }
        

        //  Getting 4 recent post/blogs 
        $recentBlogs    =   \App\Models\Blog::select('title','slug')->where('is_active',1)->take(5)->latest()->get();

        //  Returning view of blog details
        return view('blogs',compact(['blogDetails','recentBlogs']));
    }
}
