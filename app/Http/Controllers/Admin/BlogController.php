<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AddBlogRequest;
use Illuminate\Http\Request;
use App\Models\Blog;
use Storage;
use Str;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  Getting all blog list
        $blogs  =   Blog::all();

        //  Returning view of blog list
        return view('admin.blogs.blog_list',compact(['blogs']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //  Returning blog form
        return view('admin.blogs.add-new-blog-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddBlogRequest $request)
    {
        try {
            //  Creating blog model object
            $blog   =   new Blog;

            //  Updating data to related key
            $blog->title    =   trim($request->blog_title);
            $blog->content  =   trim($request->blog_content);
            $blog->slug     =   Str::of($request->blog_title)->slug('-');

            //  Saving blog image to storage folder
            $blog->image    =   Storage::put('images/blogs/',$request->file('blog_image'));

            //  Saving blog data
            $blog->save();

            //  Returning back with success message
            return back()->with('success','Blog has been added successfully.');

        } catch (\Exception $e) {
            
            //  Returning back with error message
            return back()->with('error',$e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //  Getting blog data
        $blogData   =   Blog::find(decrypt($id));

        //  Returning view with blog data
        return view('admin.blogs.edit-blog',compact(['blogData']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            //  Getting blog data object using blog model
            $blog   =   Blog::find(decrypt($id));

            //  Updating data of blog
            $blog->title    =   trim($request->blog_title);
            $blog->content  =   trim($request->blog_content);
            $blog->slug     =   Str::of($request->blog_title)->slug('-');

            //  If request has file then
            if ($request->hasFile('blog_image') && !empty($request->blog_image)) {
                
                //  Deleting previously saved image in storage folder
                Storage::delete($blog->image);

                //  Putting new image to storage folder
                $blog->image    =   Storage::put('images/blogs/',$request->file('blog_image'));
            }

            //  Saving updated data
            $blog->save();

            //  Returning back with success message
            return back()->with('success','Blog data has been updated successfully.');

        } catch (\Exception $e) {
            
            //  Returning back with server error exception message
            return back()->with('error',$e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //  Getting blog data
            $blog   =   Blog::find(decrypt($id));

            //  Deleting blog image from storage folder
            Storage::delete($blog->image);

            //  Deleting blog data
            $blog->delete();

            //  Returning success response
            return response()->json([
                'status'    =>  true,
                'title'     =>  'Deleted',
                'message'   =>  'Blog deleted successfully.'
            ]);

        } catch (\Exception $e) {
            //  Returning server error response
            return response()->json([
                'status'    =>  false,
                'title'     =>  "Internal Server Error!",
                'message'   =>  $e->getMessage()
            ]);
        }
    }
}
