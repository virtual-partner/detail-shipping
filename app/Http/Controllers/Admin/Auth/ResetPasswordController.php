<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\Admin\PasswordResetSuccess;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;

class ResetPasswordController extends Controller
{
    public function resetPasswordForm($token)
    {
        return view('admin.auth.reset-password',compact(['token']));
    }

    public function resetPassword(Request $request)
    {
        //  Validating request parameter
        $request->validate([
            'new_password'          =>  'required',
            'confirm_new_password'  =>  'required|same:new_password'
        ]);

        try {

            //  Getting password resets data using token
            $tokenData  =   \DB::table('password_resets')->where('token',$request->reset_token)->first();

            //  If token data not found then
            if (empty($tokenData)) {
                
                //  Returning back with error message
                return back()->with('error','Your token has been expired or token has been tempered. Please generate new token to reset your password.');
            }

            //  Getting admin data by using reset token's email in Admin eloquent
            $admin  =   \App\Models\Admin::where('email',$tokenData->email)->first();

            //  Assigning new password to admin object
            $admin->password    =   bcrypt($request->new_password);

            //  Saving updated password
            $admin->save();

            //  Email notification for password reset success
            $admin->notify(new PasswordResetSuccess($admin->name));
            
            //  Deleting reset token
            \DB::table('password_resets')->where('token',$tokenData->token)->delete();

            //  Returning back to login page of Admin
            return redirect()->route('admin.login.show')->with('success','Your password has been reset. Please login with your new password.');

        } catch (\Exception $e) {
            
            //  Returning with server error message
            // return back()->with('error',$e->getMessage());
            return back()->with('error','Internal Server Error! Please try again later.');
        }

    }
}
