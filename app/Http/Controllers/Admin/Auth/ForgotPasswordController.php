<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Notifications\Admin\PasswordResetLink;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
    
    public function forgotPasswordForm()
    {
        return view('admin.auth.forgot-password');
    }
    
    
    public function sendResetPasswordLink(Request $request)
    {
        //  Validating request parameter
        $request->validate([
            'email'     =>  'required|exists:admins,email'
        ]);
        
        try {
        
            //  Generating reset password token
            $resetToken     =   \Str::random(120);

            //  Inserting reset password data to password_resets table
            \DB::table('password_resets')->insert([
                'email'     =>  trim($request->email),
                'token'     =>  $resetToken,
                'created_at'=>  date('Y-m-d H:i:s')
            ]);

            //  Creating admin object of provided email
            $admin  =   \App\Models\Admin::where('email',$request->email)->first();

            //  Sending email notification
            $repsonse = $admin->notify(new PasswordResetLink($admin->name,$resetToken));
            
            //  Returning back with success message
            return back()->with('success','Password reset link has been sent.');

        } catch (\Exception $e) {
            
            //  Returning back with server error message
            return back()->with('error',$e->getMessage())->withInput();
        }
    }
}
