<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Imports\OrdersImport;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\SubAdminFormRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\OrderStatusUpdateRequest;
use App\Notifications\CustomEmailNotification;
use App\Notifications\Order\OrderCreated;
use App\Notifications\Order\OrderUpdated;

class AdminController extends Controller
{
    public function dashboard()
    {
        return view('admin.dashboard');
    }

    public function changePasswordForm()
    {
        return view('admin.profile.change-password');
    }
    
    
    public function submitChangePasswordForm(ChangePasswordRequest $request)
    {
        try {

            //  Getting password of auth user
            $adminData  =   \App\Models\Admin::find(\Auth::user()->id);

            //  Checking for current password matches or not
            if (!\Hash::check($request->current_password,$adminData->password)) {
                
                //  Returning back with current password does not match warning error message
                return back()->with('error','Your current password does not match. Please try again.');

            } else if (\Hash::check($request->new_password,$adminData->password)) {
                
                //  Returning back with current password does not match warning error message
                return back()->with('error','New password is same as current password. Please try again with different password.');

            } else {

                //  Updating new password for admin data
                $adminData->password    =   bcrypt($request->new_password);

                //  Saving updated data
                $adminData->save();

                //  Returning response with success message
                return back()->with('success','Your password has been updated successfully.');
            }
        } catch (\Exception $e) {
           
            //  Returning back with server error response
            return back()->with('error',$e->getMessage());
        }
    }

    public function orderList($type)
    {
        //  If request for current orders then
        if ($type === 'current') {
            //  Getting order list which have not been delivered yet
            $orders     =   \App\Models\Order::doesntHave('order_status','or',function($query){
                                            $query->where('order_status','delivered');
                                        })
                                        ->get();   
        } else if($type === 'delivered') {
            //  Getting order list which have not been delivered yet
            $orders     =   \App\Models\Order::whereHas('order_status',function($query){
                                            $query->where('order_status','delivered');
                                        })
                                        ->get();   
        } else {

            return view('admin.404-not-found');
        }
	
        //  Returning view with order data
        return view('admin.orders.list',compact(['orders']));
    }


    public function createOrder(CreateOrderRequest $request)
    {
        try {
            //  Beginning DB transaction
            \DB::beginTransaction();

            //  Inserting data into orders table
            $orderData  =   \App\Models\Order::create([
                                'name'                  =>  trim($request->name),
                                'email'                 =>  trim($request->email),
                                'item_name'             =>  trim($request->item_name),
                                'item_weight'           =>  trim($request->item_weight),
                                'invoice_number'        =>  trim($request->invoice_number),
                                'item_tracking_number'  =>  'TPCL'.time().mt_rand(00001,99999),
                                // 'payment_status'        =>  $request->payment_status,
                                // 'payment_amount'        =>  $request->payment_amount,
                                'shipping_address'      =>  trim($request->shipping_address),
                                'address_from'          =>  trim($request->package_details),
                                'created_at'            =>  date('Y-m-d H:i:s'),
                                'updated_at'            =>  date('Y-m-d H:i:s')
                            ]);

            //  Updating data into order_statuses table
            \App\Models\OrderStatus::insert([
                'order_id'      =>  $orderData->id,
                'order_status'  =>  'order placed',
                'comment'       =>  'Order has been placed with tracking number '.$orderData->item_tracking_number,
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ]);

            //  Sending notification to provided email
            $orderData->notify(new OrderCreated($orderData));

            //  Committing DB transaction 
            \DB::commit();

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Order data created successfully.']);

        } catch (\Exception $e) {

            //  Rolling back DB transaction
            \DB::rollback();

            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()],500);
        }

    }

    public function importOrderData(Request $request) 
    {
        $request->validate([
            'order_data_file' => 'required',
        ]);

        // try {
     
            //  Getting extension of file
            $fileExtension  =   $request->file('order_data_file')->extension();
    
            //  If file extension is not in allowed list then
            if (!in_array($fileExtension,['xlsx','csv','ods'])) {
                
                //  Returning back with error message
                return back()->with('error','Selected file format is not supported. Please select xlsx, csv, or ods format only.');
            }
            
            Excel::import(new OrdersImport ,$request->file('order_data_file'));

            return back()->with('success', 'Order data has been imported successfully.');

        // } catch (\Throwable $th) {
            
        //     //  Returning back with server error
        //     return back()->with('error','Data import failed. Please try again.');
        // }
    }

    public function editOrderData($order_id)
    {
        try {

            //  Getting order data to show on edit form
            $orderData  =   \App\Models\Order::find(decrypt($order_id));

            //  Returning view with data
            return view('admin.orders.edit-order-data',compact(['orderData']));

        } catch (\Exception $e) {
            
            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }
    
    public function updateOrderData(CreateOrderRequest $request, $order_id)
    {
        try {

            //  Creating order data object 
            $orderData  =   \App\Models\Order::find(decrypt($order_id));

            //  Assigning updated data to order data object
            $orderData->name                =   trim($request->name);
            $orderData->email               =   trim($request->email);
            $orderData->item_name           =   trim($request->item_name);
            $orderData->item_weight         =   trim($request->item_weight);
            $orderData->invoice_number      =   trim($request->invoice_number);
            $orderData->shipping_address    =   trim($request->shipping_address);
            $orderData->address_from        =   trim($request->package_details);

            //  Saving updated data
            $orderData->save();

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Order data has been updated successfully.']);

        } catch (\Exception $e) {
            
            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }


    public function updateOrderStatus(OrderStatusUpdateRequest $request, $order_id)
    {
        try {
            //  Creating empty object of order status model
            $orderStatus    =   new \App\Models\OrderStatus;

            //  Assigning values to order status object
            $orderStatus->order_id          =   decrypt($order_id);
            $orderStatus->order_status      =   trim($request->action);
            $orderStatus->comment           =   trim($request->action_comment);

            //  Saving order status data
            $orderStatus->save();

            //  Getting order data
            $orderData  =   \App\Models\Order::select('id','name','item_tracking_number')->where('id',$orderStatus->order_id)->first();

            //  Sending email notification
            $orderData->notify(new OrderUpdated($orderData, $request->action)); 
        
            //  Returning success response in JSON format
            return response()->json(['status'=>true,'message'=>'Order data created successfully.']);

        } catch (\Exception $e) {
            //  Returning server error response in JSON format
            return response()->json(['status'=>false,'message'=>$e->getMessage()],500);
        }
    }


    public function orderStatusData($order_id)
    {
        //  Getting order status data
        $orderStatus    =   \App\Models\OrderStatus::where('order_id',decrypt($order_id))->get();

        //  Returning view with data
        return view('admin.orders.status',compact(['orderStatus']));
    }

    public function contactQueries()
    {
        //  Getting contact queries
        $contact_queries    =   \App\Models\ContactQuery::all();

        //  Returning view with data
        return view('admin.contact-queries',compact(['contact_queries']));
    }
    
    public function contactQueryResponse(Request $request)
    {
        //  Validating request parameter
        $request->validate([
            'query_id'              =>  'required',
            'reply_contact_query'   =>  'required'
        ]);

        //  Getting query data
        $queryData  =   \App\Models\ContactQuery::find(decrypt($request->query_id));

        //  Adding admin response to contact query
        $queryData->admin_responded =   1;
        $queryData->admin_response  =   trim($request->reply_contact_query);

        //  Saving data
        $queryData->save();

        //  Sending mail to contact person

        //  Returning response in JSON format
        return response()->json(['status'=>true,'message'=>'Response mail has been sent successfully.']);
    }


    public function subAdmins()
    {
        
    }
    
    
    public function createSubAdmin(SubAdminFormRequest $request)
    {
        
    }
    
    
    public function activateDeactivateDeleteSubAdmin($action, $data_id)
    {
        try {
            //  Getting sub admin object 
            $subAdminData   =   \App\Models\Admin::find(decrypt($data_id));

            //  If action is to deactivate the sub admin then
            if (decrypt($action) === 'deactivate') {
                
                //  Updating sub admin status as deactivated
                $subAdminData->is_active    =   0;

            } else if(decrypt($action) === 'activate') {

                //  Updating sub admin status as active
                $subAdminData->is_active    =   1;

            } else if (decrypt($action) === 'delete') {
                
                //  Deleting sub admin's data
                $subAdminData->delete();

                //  Returning response
                return response()->json(['status'=>true,'message'=>'Sub admin data has been deleted successfully.']);

            } else {
                
                //  Returning action error response
                return response()->json(['status'=>false,'message'=>'Not able to perform action right now. Please try again later.']);
            }

            //  Saving update data of sub admin
            $subAdminData->save();

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Sub admin profile status been update.']);

        } catch (\Exception $e) {
            
            //  Returning server error response
            return response()->json(['status'=>false,'message'=>'Internal server error. Please try again after refreshing the page.']);
        }
        
    }


    public function bannerList()
    {
        //  Getting banner images list
        $bannerImages   =   \App\Models\Banner::all();

        //  Returning view with data
        return view('admin.banners.banner-list',compact(['bannerImages']));
    }
    
    public function addBanner(Request $request)
    {
        try {
            //  If banner image is not empty then
            if ($request->hasFile('banner_image') && !empty($request->banner_image)) {
                
                //  Creating banner model object
                $banner     =   new \App\Models\Banner;

                //  Moving banner image into storage and assigning path to banner object
                $banner->banner_image   =   \Storage::put('images/banners/',$request->file('banner_image'));

                //  Saving updated data
                $banner->save();
                
            }

            //  Returning view with data
            return back()->with('success','Banner has been added successfully.');

        } catch (\Exception $e) {
            
            //  Returning server error
            return back()->with('error',$e->getMessage());
        }
    }


    public function destroyBanner($banner_id)
    {
        try {
            
            //  Getting banner data
            $banner     =   \App\Models\Banner::find(decrypt($banner_id));

            //  Deleting image from storage folder
            \Storage::delete($banner->image);

            //  Deleting banner data
            $banner->delete();

            //  Returning success response
            return response()->json([
                'status'    =>  true,
                'title'     =>  'Deleted',
                'message'   =>  'Banner deleted successfully.'
            ]);

        } catch (\Exception $e) {
            //  Returning server error response
            return response()->json([
                'status'    =>  false,
                'title'     =>  "Internal Server Error!",
                'message'   =>  $e->getMessage()
            ]);
        }
    }

    public function sendCustomEmailNotification(Request $request)
    {
        $request->validate([
            'name'      =>  'required',
            'email'     =>  'required|email',
            'message'   =>  'required'
        ]);

        //  Initiating custom emal notification model object
        $customEmail    =   new \App\Models\CustomEmailNotification;

        //  Assigning values
        $customEmail->name      =   $request->name;
        $customEmail->email     =   $request->email;
        $customEmail->message   =   $request->message;

        //  Saving data
        $customEmail->save();

        //  Setting email array data
        $data   =   $request->except(['_token']);

        //  Sending notification
        $customEmail->notify(new CustomEmailNotification($data));

        //  Returning back with success response
        return back()->with('success','Email has been sent successfully.');
    }
}
