<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\Admin\SubAdminCreated;
use App\Http\Requests\SubAdminFormRequest;

class SubAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  Getting all sub admins list
        $subAdmins  =   Admin::where('role','!=',1)->get();

        //  Returning view with sub admin list
        return view('admin.sub-admin.list',compact(['subAdmins']));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubAdminFormRequest $request)
    {
        try {

            //  Beginning DB transaction
            \DB::beginTransaction();

            //  Inserting sub admin data into admins table
            $subAdmin   =   Admin::create([
                                    'name'      =>  trim($request->name),
                                    'email'     =>  trim($request->email),
                                    'password'  =>  bcrypt($request->password),
                                    'role'      =>  2,
                                    'created_at'=>  date('Y-m-d H:i:s'),
                                    'updated_at'=>  date('Y-m-d H:i:s')
                                ]);

            //  Mail to be send on the created sub admin's email id
            $subAdmin->notify(new SubAdminCreated('add', $request->name, $request->email, $request->password));

            //  Committing DB transaction 
            \DB::commit();

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Sub Admin has been created successfully.']);

        } catch (\Exception $e) {

            //  Rolling back DB transaction
            \DB::rollback();

            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {

            //  Getting sub admin's data to edit
            $subAdminData   =   Admin::find(decrypt($id));
            
            //  Returning view with data
            return view('admin.sub-admin.edit-sub-admin-data',compact(['subAdminData']));

        } catch (\Exception $e) {
            
            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubAdminFormRequest $request, $id)
    {
        try {

            //  Beginning DB transaction
            \DB::beginTransaction();

            //  Creating sub admin object by getting data of sub admin using Admin model
            $subAdmin   =   Admin::find(decrypt($id));

            //  Assigning updated values of sub admin to object
            $subAdmin->name     =   trim($request->name);
            $subAdmin->email    =   trim($request->email);

            //  If request has password then
            if ($request->has('password') && $request->password != null) {
                
                $subAdmin->password     =   bcrypt($request->password);
            }

            //  Saving updated data of sub admin
            $subAdmin->save();

            //  Mail to be send on the created sub admin's email id
            $subAdmin->notify(new SubAdminCreated('update', $request->name, $request->email, $request->password));

            //  Committing DB transaction 
            \DB::commit();

            //  Returning success response
            return response()->json(['status'=>true,'message'=>'Sub Admin data has been updated successfully.']);

        } catch (\Exception $e) {

            //  Rolling back DB transaction
            \DB::rollback();

            //  Returning server error response
            return response()->json(['status'=>false,'message'=>$e->getMessage()]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
