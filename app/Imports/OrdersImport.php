<?php

namespace App\Imports;

use App\Jobs\OrderCreated as JobsOrderCreated;
use App\Models\Order;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;
use App\Notifications\Order\OrderCreated;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\SkipsEmptyRows;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OrdersImport implements ToCollection, SkipsEmptyRows, WithHeadingRow
{
    use Importable;
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        //  Validating data
        Validator::make($rows->toArray(), [
            '*.name'            => 'required',
            '*.email'           => 'required',
            '*.item_name'       => 'required',
            '*.item_weight'     => 'required',
            '*.invoice_number'  => 'required',
            '*.shipping_address'=> 'required',
        ])->validate();

        foreach ($rows as $row) {

            $orderData  =   Order::create([
                'name'                  =>  $row['name'],
                'email'                 =>  $row['email'],
                'item_name'             =>  $row['item_name'],
                'item_weight'           =>  $row['item_weight'],
                'invoice_number'        =>  $row['invoice_number'],
                'item_tracking_number'  =>  'TPCL'.time().mt_rand(00001,99999),
                'shipping_address'      =>  $row['shipping_address'],
                'address_from'          =>  $row['package_details'],
                'created_at'            =>  date('Y-m-d H:i:s'),
                'updated_at'            =>  date('Y-m-d H:i:s')
            ]);
            
            //  Updating data into order_statuses table
            \App\Models\OrderStatus::insert([
                'order_id'      =>  $orderData->id,
                'order_status'  =>  'order placed',
                'comment'       =>  'Order has been placed with tracking number '.$orderData->item_tracking_number,
                'created_at'    =>  date('Y-m-d H:i:s'),
                'updated_at'    =>  date('Y-m-d H:i:s')
            ]);

            //  Sending email notification
            $orderData->notify(new OrderCreated($orderData));
        }
            
        // return $orderData;

    }

}
