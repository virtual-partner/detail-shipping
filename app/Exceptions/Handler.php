<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<int, class-string<Throwable>>
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<int, string>
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    /*
     *  If access token or session has expired then to handle unauthenticated request this method 
     *  redirects on the respected page
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        // If request is expecting json data (api) and access token is expired or invalid then
        if ($request->expectsJson()) 
        {
            return response()->json([
                'statusCode'    =>  401, 
                'status'        =>  $exception->getMessage(), 
                'message'       =>  'Your token has been expired. Please login again.'
            ],401);
        }
        // If simple redirect session base web pages then
        $guard = @$exception->guards()[0];
                switch ($guard) {
                    case "web" :
                        $redirectRoute = 'admin.login';
                        break;
                    default:
                        $redirectRoute = 'admin.login';
                        break;
                }
        return redirect()->guest(route($redirectRoute));
    }
}
