<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    use HasFactory;

    public function getBannerImageAttribute($banner_image)
    {
        return \Storage::url($banner_image);
    }
}
