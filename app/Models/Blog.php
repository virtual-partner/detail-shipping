<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    //  Creating blog image accessor 
    public function getImageAttribute($image)
    {
        //  Returning image after appending storage path to blog image
        return \Storage::url($image);
    }
}
