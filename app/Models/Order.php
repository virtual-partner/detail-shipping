<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Order extends Model
{
    use HasFactory, Notifiable;

    protected $fillable = ['name', 'email', 'item_name', 'item_weight', 'invoice_number', 'item_tracking_number', 'payment_status', 'payment_amount', 'shipping_address', 'address_from', 'created_at', 'updated_at'];

    protected $with     =   ['order_status'];

    public function order_status()
    {
        return $this->hasMany(OrderStatus::class);
    }
}
