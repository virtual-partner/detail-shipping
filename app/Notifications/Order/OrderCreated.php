<?php

namespace App\Notifications\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderCreated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $name;
    protected $tracking_number;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderData)
    {   
        // $this->afterCommit();
        $this->name     =   $orderData->name;
        $this->tracking_number  =   $orderData->item_tracking_number;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('SHIPPING ORDER GENERATED :: TPCL')
                    ->line('Hello '.ucfirst($this->name))
                    ->line('Your order for shipping your article has been generated successfully.')
                    ->line("Your order's tracking number is given below by using it you can track your package status.")
                    ->line("Order/Tracking Number : ".$this->tracking_number)
                    ->action('Track Package', route('homepage'))
                    ->line('Thank you for using our application!')
                    ->line('Thanks & Regards')
                    ->line('TPCL');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
