<?php

namespace App\Notifications\Order;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderUpdated extends Notification implements ShouldQueue
{
    use Queueable;
    protected $name;
    protected $tracking_number;
    protected $order_status;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($orderData, $orderStatus)
    {
        // $this->afterCommit();
        $this->name             =   $orderData->name;
        $this->tracking_number  =   $orderData->item_tracking_number;
        $this->order_status     =   $orderStatus;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('SHIPPING ORDER '.strtoupper($this->order_status).' :: TPCL')
                    ->line('Hello '.ucfirst($this->name))
                    ->line('Your order with order/tracking number '.$this->tracking_number.' has been '.$this->order_status.' successfully.')
                    ->line("You can track your order's status on our website as well. To track please click on button below.")
                    ->action('Track Package', route('homepage'))
                    ->line('Thank you for using our application!')
                    ->line('Thanks & Regards')
                    ->line('TPCL');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
