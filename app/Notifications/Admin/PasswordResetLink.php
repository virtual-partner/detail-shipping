<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasswordResetLink extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($name, $token)
    {
        $this->name     =   $name;
        $this->token    =   $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('RESET PASSWORD :: TPCL')
                    ->line(ucfirst($this->name))
                    ->line('Greetings from TPCL.')
                    ->line('Your reset password link is mentioned below. You can reset your login password by clicking on button or link below.')
                    ->action('Reset Password', route('admin.reset.password.form',['token'=>$this->token]))
                    ->line('If you have not requested to reset your password. Please ignore this mail.')
                    ->line('Thank you for using TPCL application.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
