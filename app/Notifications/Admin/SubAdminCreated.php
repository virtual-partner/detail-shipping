<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SubAdminCreated extends Notification
{
    use Queueable;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($action, $name, $email, $password=null)
    {
        $this->action   =   $action;
        $this->name     =   $name;
        $this->email    =   $email;
        $this->password =   $password;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if ($this->action === 'add') {

            return (new MailMessage)
                        ->subject('TPCL :: SUB ADMIN')
                        ->line(ucfirst($this->name))
                        ->line('Greetings from TPCL.')
                        ->line('You have been added as a sub admin in TPCL Admin portal to manage various functions of organization.')
                        ->line('Your login credentials are mentioned below :')
                        ->line('Username - '.$this->email)
                        ->line('Password - '.$this->password)
                        ->action('Click here to Login', url('/admin/login'));

        } else {
           
            //  If password has not been changed then
            if ($this->password == null) {
                return (new MailMessage)
                        ->subject('TPCL :: SUB ADMIN')
                        ->line(ucfirst($this->name))
                        ->line('Greetings from TPCL.')
                        ->line('Your personal data/login credentials has been updated by Super Admin of TPCL.')
                        ->line('You can login by credentials mentioned below with your previous password :')
                        ->line('Username - '.$this->email)
                        ->action('Click here to Login', url('/admin/login'));
            } else {
                return (new MailMessage)
                        ->subject('TPCL :: SUB ADMIN')
                        ->line(ucfirst($this->name))
                        ->line('Greetings from TPCL.')
                        ->line('Your personal data/login credentials has been updated by Super Admin of TPCL.')
                        ->line('You can login by credentials mentioned below :')
                        ->line('Username - '.$this->email)
                        ->line('Password - '.$this->password)
                        ->action('Click here to Login', url('/admin/login'));
            }
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
