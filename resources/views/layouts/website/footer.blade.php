<!--  home section serve start  -->
<div class="blog-box">
	<div class="container">
		<div class="row heading">
			<h2>Our blogs</h2>
			<p>California positions us next to the largest port in the United States.</p>
		</div>
		<div class="row blog-block">
			@php
				if(\Request::is('blogs') || \Request::is('blogs/*')){
					$blogs 	=	\App\Models\Blog::where('is_active',1)->get();
				} else {
					$blogs 	=	\App\Models\Blog::where('is_active',1)->take(3)->latest()->get();
				}
			@endphp
			@foreach ($blogs as $blog)
				<div class="col-sm-4 col-xs-12 col-md-4 col-lg-4">
					<div class="item">
						<div class="image">
							<img src="{{ $blog->image }}"/>
						</div>
						<div class="text-box">
							<h2>{{ $blog->title }}</h2>
							<p>{{ substr(strip_tags($blog->content), 0, 160) }}...</p>
							<p><a href="{{ route('blogs.details',['slug'=>$blog->slug]) }}" class="viewfulldetails">Know More</a></p>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>
<!--  home section serve end  -->

<!--  home section serve start  -->
<div class="contact-box" id="contact-form-div">
	<div class="container contact-container">
		<div class="row">
			<div class="col-md-6 address-box">
				<div class="">
					<h2>Headquarters</h2>
					<h3>ADDRESS:</h3>
					<p>1ST Crescenta Way <br>Rancho Cucamonga <br>CA ,91739</p>
					<h3>Email:</h3>
					<p>info@transpacificinternational.com</p>
					<h3>CONTACT:</h3>
					<p>(213) 766-5565</p>
				</div>
			</div>
			<div class="col-md-6 contact-us-form">
				<div class="">
					<h2>Contact Us</h2>
					<form id="contact-form" class="contact-form" onsubmit="validateAndSubmitContactForm(event)">
                        @csrf
						<div class="form-group">
							<input type="text" name="name" id="name" class="form-control" placeholder="Name"/>
						</div>
						<div class="form-group">
							<input type="text" name="email" id="email" class="form-control" placeholder="Email"/>
						</div>
						<div class="form-group">
							<textarea col="10" rows="5" name="contact_query" id="query"></textarea>
						</div>
						<div class="form-group submit-button">
							<input type="submit" name="submit" value="Submit" class="submitbtn"/>
						</div>
					</form>		
				</div>
			</div>
		</div>
	</div>
</div>
<!--  home section serve end  -->

<!--  start of footer  -->
<div class="footer-top">
	<div class="container">
		<div class="row">
			<div class="col-md-12 footer-logo">
				<img src="{{ asset('assets/images/logo.png') }}"/>
			</div>
			<div class="col-md-12">
				<div class="footer-menu">
					<ul>
						<li><a href="{{ route('services.freight.forwarding') }}">Freight Forwarding</a></li>
						<li><a href="{{ route('services.freight.ocean') }}">Ocean freight</a></li>
						<li><a href="{{ route('services.freight.air') }}">Air freight</a></li>
						<li><a href="{{ route('services.freight.trucking') }}">Trucking freight</a></li>
						<li><a href="{{ route('services.warehouse.distribution') }}">Warehouse & Distributions</a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-12">
				<div class="footer-bottom-inner">
					<p>Copyright © 2010-2021 TRANSPACIFIC CARGO AND LOGISTICS. All rights reserved.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--  end of footer  -->

<script>
    function validateAndSubmitContactForm(event) {
        event.preventDefault();
        if ($("#name").val() == '') {
            alert('Please enter name.');
        } else if ($("#email").val() == '') {
            alert('Please enter email.');
        } else if ($('textarea#query').val() == '') {
            alert('Please enter your query.');
        } else {
            $.ajax({
                url  : "{{ route('contact.query.save') }}",
                type : "POST",
                data : $("#contact-form").serialize()
            })
            .done(function(response){
				console.log(response);
				if (response.status) {
                    alert(response.message);
                    window.location.reload();
                } else {
                    alert(response.message);
                }
            })
            .fail(function(response){
        
            })
            .always(function(){

            });
        }
    }
</script>