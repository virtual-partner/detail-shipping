<div class="header-bottom">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-6 col-6">
				<div class="logo">
					<a href="{{ route('homepage') }}"><img src="{{ asset('assets/images/logo.png') }}"/></a>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-6 menu-top">
				<div class="menu-box menuArea">
					<ul class="nav">
						<li @if(\Request::is('/')) class="active" @endif><a href="{{ route('homepage') }}">Home</a></li>
						<li @if(\Request::is('about-us')) class="active" @endif><a href="{{ route('about-us') }}">About Us</a></li>
						<li @if(\Request::is('services/*')) class="active" @endif><a class="dropdown-toggle" href="#">Services</a>
							<ul>
								<li><a href="{{ route('services.freight.forwarding') }}">Freight Forwarding</a></li>
								<li><a href="{{ route('services.freight.ocean') }}">Ocean Freight</a></li>
								<li><a href="{{ route('services.freight.air') }}">Air Freight</a></li>
								<li><a href="{{ route('services.freight.trucking') }}">Trucking Freight</a></li>
								<li><a href="{{ route('services.warehouse.distribution') }}">Warehouse & Distribution</a></li>
							</ul>
						</li>
						<li @if(\Request::is('blogs/*') || \Request::is('blogs')) class="active" @endif><a href="{{ route('blogs.details') }}">Blog</a></li>
					</ul>
				</div>
				<div class="mobilemmenu">
						<a href="#mmenu" class="mbtn"><i class="fa fa-bars" aria-hidden="true"></i></a>
						<div id="mmenu">
							<ul class="nav">
								<li @if(\Request::is('/')) class="active" @endif><a href="{{ route('homepage') }}">Home</a></li>
								<li @if(\Request::is('about-us')) class="active" @endif><a href="{{ route('about-us') }}">About Us</a></li>
								<li @if(\Request::is('services/*')) class="active" @endif><a class="dropdown-toggle" href="#">Services</a>
									<ul>
										<li><a href="{{ route('services.freight.forwarding') }}">Freight Forwarding</a></li>
										<li><a href="{{ route('services.freight.ocean') }}">Ocean Freight</a></li>
										<li><a href="{{ route('services.freight.air') }}">Air Freight</a></li>
										<li><a href="{{ route('services.freight.trucking') }}">Trucking Freight</a></li>
										<li><a href="{{ route('services.warehouse.distribution') }}">Warehouse & Distribution</a></li>
									</ul>
								</li>
								<li @if(\Request::is('blogs/*') || \Request::is('blogs')) class="active" @endif><a href="{{ route('blogs.details') }}">Blog</a></li>
								<li><a href="#contact-form-div" class="contact-us-btn">Contact Us</a></li>
							</ul>
						</div>
					</div>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-12">
				<div class="header-contact">
					<a href="#contact-form-div" class="contact-us-btn">Contact Us</a>
				</div>
			</div>
		</div>
	</div>
</div>