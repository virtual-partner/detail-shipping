<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>@yield('title') | TPCL</title>
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Lato:ital,wght@0,100;0,300;0,400;0,700;0,900;1,100;1,300;1,400;1,700;1,900&family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&family=Poppins:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">

<link href="{{ asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('admin_assets/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('admin_assets/css/style.css') }}" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/jquery.mmenu.all.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/owl.carousel.css') }}"/>
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/style.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/responsive.css') }}"/>
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

<script type="text/javascript" src="{{ asset('assets/js/jquery.3.3.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.mmenu.min.all.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/owl.carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/global.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

</head>
<body>
@include('layouts.website.header')
@yield('content')
@include('layouts.website.footer')
</body>
</html>