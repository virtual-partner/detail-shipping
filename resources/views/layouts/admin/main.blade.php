<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TPCL | @yield('title')</title>

    <link href="{{ asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('admin_assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/css/style.css') }}" rel="stylesheet">

    <!-- Sweet Alert -->
    <link href="{{ asset('admin_assets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <script src="{{ asset('admin_assets/js/jquery-3.1.1.min.js') }}"></script>

</head>

<body class="">

    <div id="wrapper">
        @include('layouts.admin.partials.sidebar')

        <div id="page-wrapper" class="gray-bg">
            @include('layouts.admin.partials.header')

            @yield('content')

            @include('layouts.admin.partials.footer')
        </div>
    </div>

<!-- Mainly scripts -->
    <script src="{{ asset('admin_assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin_assets/js/bootstrap.js') }}"></script>
    <script src="{{ asset('admin_assets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('admin_assets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Sweet alert -->
    <script src="{{ asset('admin_assets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

    <!-- Custom and plugin javascript -->
    <script src="{{ asset('admin_assets/js/inspinia.js') }}"></script>
    <script src="{{ asset('admin_assets/js/plugins/pace/pace.min.js') }}"></script>


</body>

</html>