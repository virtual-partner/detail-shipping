<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                    <img alt="image" class="rounded-circle" src="{{ asset('admin_assets/img/profile_small.jpg') }}"/>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="block m-t-xs font-bold">{{ Auth::user()->name }}</span>
                        <span class="text-muted text-xs block">Admin <b class="caret"></b></span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <!-- <li><a class="dropdown-item" href="#">Profile</a></li> -->
                        <li><a class="dropdown-item" href="{{ route('admin.update.password.form') }}">Change Password</a></li>
                        <!-- <li><a class="dropdown-item" href="#">Mailbox</a></li> -->
                        <li class="dropdown-divider"></li>
                        <li>
                            <a class="dropdown-item" href="javascript:void(0)" onclick="event.preventDefault(); document.getElementById('admin-logout-form').submit();">Logout</a>
                            <form id="admin-logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            
            <li @if (\Request::is('admin/dashboard')) class="active" @endif>
                <a href="{{ route('admin.dashboard') }}"><i class="fa fa-diamond"></i> <span class="nav-label">DASHBOARD</span></a>
            </li>
            
            @if (Auth::user()->role == 1)
                <li @if (\Request::is('admin/sub/admin')) class="active" @endif>
                    <a href="{{ route('admin.sub.admin.index') }}"><i class="fa fa-users"></i> <span class="nav-label">SUB ADMINS</span>  </a>
                </li>
            @endif
            
            <li @if (\Request::is('admin/order/*')) class="active" @endif>
                <a href="{{ route('admin.order.list',['type'=>'current']) }}"><i class="fa fa-list-ul"></i> <span class="nav-label">CURRENT ORDERS</span>  </a>
            </li>
            
            <li @if (\Request::is('admin/order/*')) class="active" @endif>
                <a href="{{ route('admin.order.list',['type'=>'delivered']) }}"><i class="fa fa-list-ul"></i> <span class="nav-label">DELIVERED ORDERS</span>  </a>
            </li>
            
            <li @if (\Request::is('admin/contact/*')) class="active" @endif>
                <a href="{{ route('admin.contact.queries') }}"><i class="fa fa-question"></i> <span class="nav-label">CONTACT QUERIES</span>  </a>
            </li>
            
            <li @if (\Request::is('admin/blogs/*')) class="active" @endif>
                <a href="{{ route('admin.blogs.index') }}"><i class="fa fa-file"></i> <span class="nav-label">BLOGS</span>  </a>
            </li>
            
            <li @if (\Request::is('admin/custom/email/notification')) class="active" @endif>
                <a href="{{ route('admin.custom.email.notification') }}"><i class="fa fa-email"></i> <span class="nav-label">CUSTOM EMAIL</span>  </a>
            </li>
            
            {{-- <li @if (\Request::is('admin/banners/*')) class="active" @endif>
                <a href="{{ route('admin.banners.list') }}"><i class="fa fa-list"></i> <span class="nav-label">BANNERS</span>  </a>
            </li> --}}
        </ul>

    </div>
</nav>
