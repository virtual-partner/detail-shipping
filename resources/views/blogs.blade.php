@extends('layouts.website.main')
@section('title','Blogs')
@section('content')
<!--  home section serve start  -->
<div class="blog-page-box">
	<div class="container">
		<div class="row">
			<div class="col-md-8 blog-left">
				<div class="blog-text">
					<div class="blog-image">
						<img src="{{ $blogDetails->image }}"/>
					</div>
					<h2>{{ $blogDetails->title }}</h2>
					<p>{!! $blogDetails->content !!}</p>
				</div>
			</div>
			<div class="col-md-4 blog-right">
				<div class="recent-post">
					<h2>Recent Posts</h2>
					<ul>
						@foreach ($recentBlogs as $recentBlog)
							<li>
								<a href="{{ route('blogs.details',['slug'=>$recentBlog->slug]) }}">{{ $recentBlog->title }}</a>
							</li>
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<!--  home section serve end  -->
@endsection