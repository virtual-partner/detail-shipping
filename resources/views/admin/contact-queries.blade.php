@extends('layouts.admin.main')
@section('title','Contact Queries')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Contact Query List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Contact Queries</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Contact Query list with data</h5>
                    <!-- <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul>
                        <button type="button" href="javascript:void()" data-target="#add-order-data-modal" data-toggle="modal" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                            Add Order
                        </button>
                    </div> -->
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name </th>
                    <th>Email</th>
                    <th>Query</th>
                    <th>Query Date/Time</th>
                    <th>Responded</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($contact_queries as $key => $query)
                        <tr class="gradeX">
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $query->name }}
                            </td>
                            <td>
                                {{ $query->email }}
                            </td>
                            <td>
                                {{ $query->query }}
                            </td>
                            <td>
                                {{ date('d M Y H:i',strtotime($query->created_at)) }}
                            </td>
                            <td>
                                @if ($query->admin_responded)
                                    <span class="badge badge-success">Responded</span>
                                @else
                                    <span class="badge badge-warning">Pending</span>
                                    
                                @endif
                            </td>
                            <td>

                                <a href="javascript:void(0)" onclick="showQueryDataModal('{{ $query->query }}','{{ encrypt($query->id) }}','{{$query->admin_response}}')">
                                    <i class="fa fa-eye"></i>
                                </a>
                                &nbsp;|&nbsp;
                                <a href="javascript:void(0)" onclick="showContactQueryReplyFormModal('{{ encrypt($query->id) }}')">
                                    <i class="fa fa-mail-reply"></i>
                                </a>
                                <!-- &nbsp;|&nbsp;
                                <a href="javascript:void()">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                &nbsp;|&nbsp; -->
                                <!-- <a href="javascript:void()" title="Update Status" onclick="showOrderStatusUpdateModal('{{ route('admin.contact.query.response',['query_id'=>encrypt($query->id)]) }}')">
                                    <i class="fa fa-plus-square"></i>
                                </a> -->
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name </th>
                    <th>Email</th>
                    <th>Query</th>
                    <th>Query Date/Time</th>
                    <th>Responded</th>
                    <th>Action</th>
                </tr>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="reply-contact-query-form-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Reply Contact Query </h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="contact-query-response-form">
                        @csrf
                        <input type="hidden" name="query_id" id="query_id" value="">
                        <div class="form-group  row">
                            <label class="col-sm-3 col-form-label">Reply Message :</label>
                            <div class="col-sm-9">
                                <textarea type="text" name="reply_contact_query" id="reply_contact_query" rows="8" class="form-control" placeholder="Response message goes here ..."></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                        
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="validateAndSubmitContactQueryReplyForm()">Submit</button>
                </div>
            </div>
        </div>
    </div>
    
    <!-- Order Status update Modal -->
    <div class="modal inmodal fade" id="contact-query-data-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Contact Query</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <input type="hidden" id="contact_query_id" value="">
                    <div class="form-group  row">
                        <label class="col-sm-2 col-form-label">Query :</label>
                        <div class="col-sm-10">
                            <textarea type="text" id="contact_query" rows="6" readonly class="form-control" placeholder="Response message goes here ..."></textarea>
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group row" id="contact_query_response" style="display: none;">
                        <label class="col-sm-2 col-form-label">Response :</label>
                        <div class="col-sm-10">
                            <textarea type="text" id="admin_response" rows="6" readonly class="form-control" placeholder="Response message goes here ..."></textarea>
                        </div>
                    </div>
                </div>
                    
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="showContactQueryReplyFormModal()">Reply</button>
                </div>
            </div>
        </div>
    </div>

<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

        function showQueryDataModal(query_message, query_id, admin_response) {
            $("textarea#contact_query").val('');
            $("textarea#admin_response").val('');
            $("#contact_query_response").hide();
            $("textarea#contact_query").val(query_message);
            if (admin_response != '') {
                
                $("textarea#admin_response").val(admin_response);
                $("#contact_query_response").show();
            }
            $("#contact_query_id").val(query_id);
            $("#contact-query-data-modal").modal('toggle');
        }

        function showContactQueryReplyFormModal(query_id=null) {
            $("textarea#reply_contact_query").val();
            $("#contact-query-data-modal").modal('hide');
            if (query_id == null) {
                $("#query_id").val($("#contact_query_id").val());
            } else {
                $("#query_id").val(query_id);
            }
            $("#reply-contact-query-form-modal").modal('toggle');
        }

        function validateAndSubmitContactQueryReplyForm() {
            $.ajax({
                url  : "{{ route('admin.contact.query.response') }}",
                type : "POST",
                data : $("#contact-query-response-form").serialize()
            })
            .done(function(response){
                console.log(response);
                window.location.reload();
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
        
        function validateAndSubmitOrderStatusUpdateForm() {
            $.ajax({
                url  : $("#contact-query-response-form").attr('action'),
                type : "PUT",
                data : $("#contact-query-response-form").serialize()
            })
            .done(function(response){
                console.log(response);
                window.location.reload();
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
    </script>
@endsection