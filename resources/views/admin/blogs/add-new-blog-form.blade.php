@extends('layouts.admin.main')
@section('title','Add Blog')
@section('content')

<link href="{{ asset('admin_assets/css/plugins/summernote/summernote-bs4.css') }}" rel="stylesheet">
<!-- SUMMERNOTE -->
<script src="{{ asset('admin_assets/js/plugins/summernote/summernote-bs4.js') }}"></script>

<style>
    .validation_error_message{
        color: red;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Add New Blog</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="{{ route('admin.blogs.index') }}">Blogs</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Add Blog</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('success'))
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <strong>
                        Success!
                    </strong>
                    {{ session('success') }}
                </div>
            @elseif (session()->has('error'))
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <strong>
                        Error!
                    </strong>
                    {{ session('error') }}
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Add Blog Form</h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ route('admin.blogs.index') }}" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                            Blog List
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="add-blog-form" action="{{ route('admin.blogs.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Blog Title :</label>
                                    <div class="col-sm-8">
                                        <input type="text" name="blog_title" id="blog_title" class="form-control" placeholder="Blog Title" value="{{ old('blog_title') }}">
                                        @if ($errors->has('blog_title'))
                                            <span class="validation_error_message">{{ $errors->first('blog_title') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Blog Image :</label>
                                    <div class="col-sm-8">
                                        <input type="file" name="blog_image" id="blog_image" class="form-control">
                                        @if ($errors->has('blog_image'))
                                            <span class="validation_error_message">{{ $errors->first('blog_image') }}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="hr-line-dashed"></div>
                                <div class="form-group  row">
                                    <label class="col-sm-4 col-form-label">Blog Content :</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control summernote-editor" rows="8" name="blog_content" id="blog_content" class="form-control" placeholder="Blog content goes here ...">{{ old('blog_content') }}</textarea>
                                        @if ($errors->has('blog_content'))
                                            <span class="validation_error_message">{{ $errors->first('blog_content') }}</span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="hr-line-dashed"></div>
                                <div class="form-group  row">
                                    <div class="col-sm-12">
                                        <input type="submit" class="btn btn-primary">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){

        $('.summernote-editor').summernote({
                                    height: 250,
                                    focus: true
                                });

    });
</script>

@endsection