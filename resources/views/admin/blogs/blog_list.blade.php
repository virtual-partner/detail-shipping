@extends('layouts.admin.main')
@section('title','Blog List')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Blog List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Blogs</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if(session()->has('success'))
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Success
                        </strong>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Error !
                        </strong>
                        {{ session('error') }}
                    </div>
                </div>
            @endif
            <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Blog list with data</h5>
                    <div class="ibox-tools">
                        <a type="button" href="{{ route('admin.blogs.create') }}" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                            Add Blog
                        </a>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Blog Title </th>
                    <th>Blog Slug</th>
                    <th>Blog Image</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($blogs as $key => $blog)
                        <tr class="gradeX">
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ $blog->title }}
                            </td>
                            <td>
                                {{ $blog->slug }}
                            </td>
                            <td style="align-items: center; text-align: center;">
                                <a href="javascript:void(0)" onclick="showBlogImage('{{ $blog->image }}')">
                                    <img src="{{ $blog->image }}" height="50px" width="100px" alt="">
                                </a>
                            </td>
                            
                            <td>
                                @if ($blog->is_active)
                                    <span class="badge badge-success">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <a href="javascript:void(0)" title="View Content" onclick="showBlogContent({{ json_encode($blog) }})">
                                    <i class="fa fa-eye"></i>
                                </a>
                                &nbsp;|&nbsp;
                                <a href="{{ route('admin.blogs.edit',['blog'=>encrypt($blog->id)]) }}" title="Edit Blog">
                                    <i class="fa fa-pencil"></i>
                                </a>
                                &nbsp;|&nbsp;
                                <a href="javascript:void()" title="Delete" onclick="deleteBlog('{{ route('admin.blogs.destroy',['blog'=>encrypt($blog->id)]) }}')">
                                    <i class="fa fa-trash" style="color:red;"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                <th>Sr. No.</th>
                    <th>Blog Title </th>
                    <th>Blog Slug</th>
                    <th>Blog Image</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>

    <!-- Blog Data Modal -->
    <div class="modal inmodal fade" id="blog-data-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="blog-title-text"></h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12" id="main-data-div">
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: []

            });

        });

        function showBlogContent(data) {
            $("#main-data-div").empty();
            $("#blog-title-text").text("Blog Content");
            $("#main-data-div").append("<p id='blog-content-text'>"+data.content+"</p>");
            $("#blog-data-modal").modal('toggle');
        }

        function showBlogImage(blog_image) {
            $("#main-data-div").empty();
            $("#blog-title-text").text('Blog Image');
            $("#main-data-div").append("<img id='blog-image' src='"+blog_image+"'>");
            $("#blog-image").css({
                                "max-width": "100%",
                                "max-height": "100%",
                                "display": "block" 
                            });
            $("#blog-data-modal").modal('toggle');

        }

        function deleteBlog(url) {
            var csrf_token  =   "{{ csrf_token() }}";
            swal({
                title: "Are you sure?",
                text: "You won't be able to recover the deleted blog!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url  : url,
                        type : "DELETE",
                        data : {_token:csrf_token}
                    })
                    .done(function(response){
                        if (response.status) {
                            swal({
                                title : response.title,
                                text : response.message,
                                type : "success"
                            }, function(){
                                window.location.reload();
                            });
                        } else {
                            swal({
                                title : response.title,
                                text  : response.message,
                                type  : "error"
                            });
                        }
                    })
                    .fail(function(response){
                        console.log('fail');
                    })
                    .always(function(){
                        console.log('complete');
                    });
                }
            });
        }
    </script>
@endsection