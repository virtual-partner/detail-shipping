@extends('layouts.admin.main')
@section('title','Sub Admin List')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Sub Admin List</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Sub Admins</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Sub Admin list with data</h5>
                <div class="ibox-tools">
                    <!-- <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="dropdown-item">Config option 1</a>
                        </li>
                        <li><a href="#" class="dropdown-item">Config option 2</a>
                        </li>
                    </ul> -->
                    <button type="button" href="javascript:void()" data-target="#add-sub-admin-data-modal" data-toggle="modal" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                        Add Sub Admin
                    </button>
                </div>
            </div>
            <div class="ibox-content">

                <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover dataTables-example" >
            <thead>
            <tr>
                <th>Sr. No.</th>
                <th>Name </th>
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($subAdmins as $key => $sub_admin)
                    <tr class="gradeX">
                        <td>
                            {{ $key+1 }}
                        </td>
                        <td>
                            {{ $sub_admin->name }}
                        </td>
                        <td>
                            {{ $sub_admin->email }}
                        </td>
                        <td>
                            @if ($sub_admin->is_active)
                                <span class="badge badge-success">Active</span>
                            @else
                                <span class="badge badge-warning">Inactive</span>
                            @endif
                        </td>
                        <td>
                            <!-- <a href="javascript:void(0)" title="View Order Status" onclick="showOrderStatusData('{{ route('admin.order.status.data',['order_id'=>encrypt($sub_admin->id)]) }}')">
                                <i class="fa fa-eye"></i>
                            </a>
                            &nbsp;|&nbsp; -->
                            <a href="javascript:void(0)" title="Edit Data" onclick="getSubAdminDataToEdit('{{ route('admin.sub.admin.edit',['id'=>encrypt($sub_admin->id)]) }}')">
                                <i class="fa fa-pencil"></i>
                            </a>
                            &nbsp;|&nbsp;
                            <a href="javascript:void(0)" title="Update Status" onclick="activateDeactivateDeleteSubAdminData('{{ route('admin.sub.admin.activate.deactivate.delete',['action'=>encrypt('delete'),'data_id'=>encrypt($sub_admin->id)]) }}')">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr>
            <th>Sr. No.</th>
                <th>Name </th>
                <th>Email</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </tfoot>
            </table>
                </div>

            </div>
        </div>
    </div>
    </div>
</div>

<div class="modal inmodal fade" id="add-sub-admin-data-modal" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Add Sub Admin</h4>
                <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
            </div>
            <div class="modal-body">
                <form id="add-sub-admin-form">
                    @csrf
                    <input type="hidden" name="action" value="add">
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Name :</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" id="name" class="form-control" placeholder="Enter Name">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Email :</label>
                        <div class="col-sm-8">
                            <input type="email" name="email" id="email" class="form-control" placeholder="Enter Email">
                        </div>
                    </div>
                    <div class="hr-line-dashed"></div>
                    <div class="form-group  row">
                        <label class="col-sm-4 col-form-label">Password :</label>
                        <div class="col-sm-8">
                            <input type="text" name="password" id="password" class="form-control" placeholder="Enter Password">
                        </div>
                    </div>
                </div>
                
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <button type="button" onclick="validateAndSubmitForm()" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal to Edit Sub Admin Data -->
<div class="modal inmodal fade" id="edit-sub-admin-data-modal" tabindex="-1" role="dialog"  aria-hidden="true"></div>

<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                ]

            });

        });

        function validateAndSubmitForm() {
            $("#add-sub-admin-data-modal").modal('toggle');
            $.ajax({
                url  : "{{ route('admin.sub.admin.store') }}",
                type : "POST",
                data : $("#add-sub-admin-form").serialize()
            })
            .done(function(response){
                if (response.status) {
                    swal({
                        title : "Added",
                        text : response.message,
                        type : "success"
                    }, function(){
                        window.location.reload();
                    });
                } else {
                    swal({
                        title : "Error!",
                        text : response.message,
                        type : "error"
                    });
                }
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }

        function getSubAdminDataToEdit(url) {
            $.ajax({
                url  : url,
                type : "GET"
            })
            .done(function(response){
                $("#edit-sub-admin-data-modal").html(response);
                $("#edit-sub-admin-data-modal").modal('toggle');
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
       
        function validateAndSubmitUpdateForm() {
            $("#edit-sub-admin-data-modal").modal('toggle');
            $.ajax({
                url  : $("#edit-sub-admin-form").attr('action'),
                type : "PUT",
                data : $("#edit-sub-admin-form").serialize()
            })
            .done(function(response){
                console.log(response);
                alert(response.message);
                window.location.reload();
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
        
        function activateDeactivateDeleteSubAdminData(url) {
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover deleted sub admin data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url  : url,
                    type : "GET"
                })
                .done(function(response){
                    console.log(response);
                    if (response.status) {
                        swal({
                            title : "Deleted",
                            text : response.message,
                            type : "success"
                        }, function(){
                            window.location.reload();
                        });
                    } else {
                        swal({
                            title : "Error!",
                            text : response.message,
                            type : "error"
                        });
                    }
                })
                .fail(function(response){
                    $.each(response.responseJSON.errors,function(index,value){
                        alert(value);
                    });
                })
                .always(function(){

                });
            });
        }


        function showOrderStatusUpdateModal(action_url) {
            $("#update-order-status-form").attr('action','');
            $("#update-order-status-form").attr('action',action_url);
            $("#order-status-update-modal").modal('toggle');
        }
        
        
        function validateAndSubmitOrderStatusUpdateForm() {
            $.ajax({
                url  : $("#update-order-status-form").attr('action'),
                type : "PUT",
                data : $("#update-order-status-form").serialize()
            })
            .done(function(response){
                console.log(response);
                alert(response.message);
                window.location.reload();
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }


        function showOrderStatusData(url) {
            $.ajax({
                url  : url,
                type : "GET"
            })
            .done(function(response){
                $("#order-status-data-modal").html(response);
                $("#order-status-data-modal").modal('toggle');
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
    </script>
@endsection