<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Edit Sub Admin</h4>
            <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
        </div>
        <div class="modal-body">
            <form id="edit-sub-admin-form" action="{{ route('admin.sub.admin.update',['id'=>encrypt($subAdminData->id)]) }}">
                @method('PUT')
                @csrf
                <input type="hidden" name="action" value="edit">
                <input type="hidden" name="admin_id" value="{{ encrypt($subAdminData->id) }}">
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Name :</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="name" class="form-control" value="{{ $subAdminData->name }}" placeholder="Enter Name">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Email :</label>
                    <div class="col-sm-8">
                        <input type="email" name="email" id="email" class="form-control" value="{{ $subAdminData->email }}" placeholder="Enter Email">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Password :</label>
                    <div class="col-sm-8">
                        <input type="text" name="password" id="password" class="form-control" placeholder="Enter Password">
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" onclick="validateAndSubmitUpdateForm()" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
</div>