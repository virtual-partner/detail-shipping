@extends('layouts.admin.main')
@section('title','Order List')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Order List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Orders</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if(session()->has('success'))
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Success
                        </strong>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Error !
                        </strong>
                        {{ session('error') }}
                    </div>
                </div>
            @endif
            @if (count($errors) > 0)
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <h4><i class="icon fa fa-ban"></i> Error!</h4>
                        @foreach($errors->all() as $error)
                          {{ $error }} <br>
                          @endforeach  
                    </div>
                </div>
                
            @endif
            <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Order list with data</h5>
                    <div class="ibox-tools">
                        <!-- <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-wrench"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li><a href="#" class="dropdown-item">Config option 1</a>
                            </li>
                            <li><a href="#" class="dropdown-item">Config option 2</a>
                            </li>
                        </ul> -->
                        <button type="button" href="javascript:void()" data-target="#import-order-data-modal" data-toggle="modal" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                            Import Order
                        </button>

                        @if (\Request::is('admin/order/list/current'))
                            <button type="button" href="javascript:void()" data-target="#add-order-data-modal" data-toggle="modal" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                                Add Order
                            </button>
                        @endif
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Tracking Number </th>
                    <th>Item Name</th>
                    <th>Package Weight (Kg)</th>
                    <!-- <th>Payment Status</th>
                    <th>Payment Amount</th> -->
                    <th>Invoice Number</th>
                    <th>Shipping Address</th>
                    <th>Current Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $key => $order)
                        <tr class="gradeX">
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                {{ ucfirst($order->name) }}
                            </td>
                            <td>
                                {{ $order->email }}
                            </td>
                            <td>
                                {{ $order->item_tracking_number }}
                            </td>
                            <td>
                                {{ $order->item_name }}
                            </td>
                            <td>
                                {{ $order->item_weight }}
                            </td>
                            <!-- <td>
                                @if ($order->payment_status)
                                    <span class="badge badge-primary">Paid</span>
                                @else
                                    <span class="badge badge-warning">Pending</span>
                                @endif
                            </td>
                            <td>
                                {{ $order->payment_amount }}
                            </td> -->
                            <td>
                                {{ $order->invoice_number }}
                            </td>
                            
                            <td>
                                {{ $order->shipping_address }}
                            </td>
                            <td>
                                {{ ucwords($order->order_status->sortByDesc('created_at')->first()->order_status) }}
                            </td>
                            <td>
                                <a href="javascript:void(0)" title="View Order Status" onclick="showOrderStatusData('{{ route('admin.order.status.data',['order_id'=>encrypt($order->id)]) }}')">
                                    <i class="fa fa-eye"></i>
                                </a>
                                @if (empty($order->order_status->where('order_status','DELIVERED')->toArray()))
                                    &nbsp;|&nbsp;
                                    <a href="javascript:void(0)" title="Edit Order" onclick="getOrderDataToEdit('{{ route('admin.order.edit',['order_id'=>encrypt($order->id)]) }}')">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    &nbsp;|&nbsp;
                                    <a href="javascript:void(0)" title="Update Status" onclick="showOrderStatusUpdateModal('{{ route('admin.order.update.status',['order_id'=>encrypt($order->id)]) }}')">
                                        <i class="fa fa-plus-square"></i>
                                    </a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Sr. No.</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Tracking Number </th>
                    <th>Item Name</th>
                    <th>Package Weight (Kg)</th>
                    <!-- <th>Payment Status</th>
                    <th>Payment Amount</th> -->
                    <th>Invoice Number</th>
                    <th>Shipping Address</th>
                    <th>Current Status</th>
                    <th>Action</th>
                </tr>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="import-order-data-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Import Order Data</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="import-order-data-form" action="{{ route('admin.order.import.data') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Select Excel file to Upload :</label>
                            <div class="col-sm-8">
                                <input type="file" name="order_data_file" id="order_data_file" class="form-control">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <div class="col-sm-12">
                                <a href="{{ asset('assets/Sample Doc/Sample Order Data Sheet.xlsx') }}" style="cursor: pointer;">
                                    Download sample sheet to upload order data
                                </a>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Import Data</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    
    <div class="modal inmodal fade" id="add-order-data-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Add Order</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="add-order-data-form">
                        @csrf
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Name :</label>
                            <div class="col-sm-8">
                                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Email :</label>
                            <div class="col-sm-8">
                                <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Item Name :</label>
                            <div class="col-sm-8">
                                <input type="text" name="item_name" id="item_name" class="form-control" placeholder="Item Name">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Item Weight (Kg) :</label>
                            <div class="col-sm-8">
                                <input type="text" name="item_weight" id="item_weight" class="form-control" placeholder="Item Weight (In Kg)">
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Invoice Number :</label>
                            <div class="col-sm-8">
                                <input type="text" name="invoice_number" id="invoice_number" class="form-control" placeholder="Invoice Number">
                            </div>
                        </div>

                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Shipping Address :</label>
                            <div class="col-sm-8">
                                <textarea type="text" name="shipping_address" id="shipping_address" class="form-control" placeholder="Shipping Address ..."></textarea>
                            </div>
                        </div>
                        
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Package Details :</label>
                            <div class="col-sm-8">
                                <textarea type="text" name="package_details" id="package_details" class="form-control" placeholder="Package Details ..."></textarea>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" onclick="validateAndSubmitForm()" class="btn btn-primary">Save Order</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal inmodal fade" id="edit-order-data-modal" tabindex="-1" role="dialog"  aria-hidden="true"></div>
    
    <!-- Order Status update Modal -->
    <div class="modal inmodal fade" id="order-status-update-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Update Order Status</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="update-order-status-form">
                        @csrf
                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label">Action</label>
                            <div class="col-sm-8">
                                <select class="form-control m-b" name="action" id="action">
                                    <!-- <option value="Order Placed">Order Placed</option> -->
                                    <option value="PICKED UP">PICKED UP</option>
                                    <option value="DISPATCHED">DISPATCHED</option>
                                    <option value="IN TRANSIT">IN TRANSIT</option>
                                    <option value="REACHED TO NEAREST HUB">REACHED TO NEAREST HUB</option>
                                    <option value="OUT FOR DELIVERY">OUT FOR DELIVERY</option>
                                    <option value="DELIVERED">DELIVERED</option>
                                    <option value="ON HOLD">ON HOLD</option>
                                    <option value="RETURNED">RETURNED</option>
                                </select>
                            </div>
                        </div>
                        <div class="hr-line-dashed"></div>
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Action Comment :</label>
                            <div class="col-sm-8">
                                <textarea type="text" name="action_comment" id="action_comment" class="form-control" placeholder="Comment goes here ..."></textarea>
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="button" onclick="validateAndSubmitOrderStatusUpdateForm()" class="btn btn-primary">Update Status</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Order Status Modal -->
    <div class="modal inmodal fade" id="order-status-data-modal" tabindex="-1" role="dialog"  aria-hidden="true"></div>

<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                ]

            });

        });

        function validateAndSubmitForm() {
            $.ajax({
                url  : "{{ route('admin.order.create') }}",
                type : "POST",
                data : $("#add-order-data-form").serialize()
            })
            .done(function(response){
                if (response.status) {
                    swal({
                        title : "Added",
                        text : response.message,
                        type : "success"
                    }, function(){
                        window.location.reload();
                    });
                } else {
                    swal({
                        title : "Error!",
                        text : response.message,
                        type : "error"
                    });
                }
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }


        function showOrderStatusUpdateModal(action_url) {
            $("#update-order-status-form").attr('action','');
            $("#update-order-status-form").attr('action',action_url);
            $("#order-status-update-modal").modal('toggle');
        }


        function getOrderDataToEdit(url) {
            $.ajax({
                url  : url,
                type : "GET"
            })
            .done(function(response){
                $("#edit-order-data-modal").html(response);
                $("#edit-order-data-modal").modal('toggle');
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }

        function validateAndSubmitOrderUpdateForm() {
            $.ajax({
                url  : $("#edit-order-data-form").attr('action'),
                type : "PUT",
                data : $("#edit-order-data-form").serialize()
            })
            .done(function(response){
                if (response.status) {
                    swal({
                        title : "Updated",
                        text : response.message,
                        type : "success"
                    }, function(){
                        window.location.reload();
                    });
                } else {
                    swal({
                        title : "Error!",
                        text : response.message,
                        type : "error"
                    });
                }
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
        
        
        function validateAndSubmitOrderStatusUpdateForm() {
            $.ajax({
                url  : $("#update-order-status-form").attr('action'),
                type : "PUT",
                data : $("#update-order-status-form").serialize()
            })
            .done(function(response){
                if (response.status) {
                    swal({
                        title : "Updated",
                        text : response.message,
                        type : "success"
                    }, function(){
                        window.location.reload();
                    });
                } else {
                    swal({
                        title : "Error!",
                        text : response.message,
                        type : "error"
                    });
                }
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }


        function showOrderStatusData(url) {
            $.ajax({
                url  : url,
                type : "GET"
            })
            .done(function(response){
                $("#order-status-data-modal").html(response);
                $("#order-status-data-modal").modal('toggle');
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
        }
    </script>
@endsection