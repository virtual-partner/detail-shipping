<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title">Edit Order</h4>
            <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
        </div>
        <div class="modal-body">
            <form id="edit-order-data-form" action="{{ route('admin.order.update',['order_id'=>encrypt($orderData->id)]) }}">
                @csrf
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Name :</label>
                    <div class="col-sm-8">
                        <input type="text" name="name" id="name" value="{{ $orderData->name }}" class="form-control" placeholder="Name">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Email :</label>
                    <div class="col-sm-8">
                        <input type="email" name="email" id="email" value="{{ $orderData->name }}" class="form-control" placeholder="Email">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Item Name :</label>
                    <div class="col-sm-8">
                        <input type="text" name="item_name" id="item_name" value="{{ $orderData->item_name }}" class="form-control" placeholder="Item Name">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Item Weight (Kg) :</label>
                    <div class="col-sm-8">
                        <input type="text" name="item_weight" id="item_weight" value="{{ $orderData->item_weight }}" class="form-control" placeholder="Item Weight (In Kg)">
                    </div>
                </div>
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Invoice Number :</label>
                    <div class="col-sm-8">
                        <input type="text" name="invoice_number" id="invoice_number" value="{{ $orderData->invoice_number }}" class="form-control" placeholder="Invoice Number">
                    </div>
                </div>

                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Shipping Address :</label>
                    <div class="col-sm-8">
                        <textarea type="text" name="shipping_address" id="shipping_address" value="{{ $orderData->shipping_address }}" class="form-control" placeholder="Shipping Address ...">{{ $orderData->shipping_address }}</textarea>
                    </div>
                </div>
                
                <div class="hr-line-dashed"></div>
                <div class="form-group  row">
                    <label class="col-sm-4 col-form-label">Package Details :</label>
                    <div class="col-sm-8">
                        <textarea type="text" name="package_details" id="package_details" value="{{ $orderData->address_from }}" class="form-control" placeholder="Package Details ...">{{ $orderData->address_from }}</textarea>
                    </div>
                </div>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" onclick="validateAndSubmitOrderUpdateForm()" class="btn btn-primary">Update Order</button>
            </div>
        </form>
    </div>
</div>