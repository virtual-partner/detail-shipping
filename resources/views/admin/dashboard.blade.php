@extends('layouts.admin.main')
@section('title','Dashboard')
@section('content')

    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <!-- <span class="label label-success float-right">Monthly</span> -->
                        <h5>Orders</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ \App\Models\Order::all()->count() }}</h1>
                        <!-- <div class="stat-percent font-bold text-success">98% <i class="fa fa-bolt"></i></div> -->
                        <small>Total orders</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <!-- <span class="label label-info float-right">Annual</span> -->
                        <h5>Orders Delivered</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ \App\Models\Order::whereHas('order_status',function($query){
                            $query->where('order_status','delivered'); })->get()->count() }}</h1>
                        <!-- <div class="stat-percent font-bold text-info">20% <i class="fa fa-level-up"></i></div> -->
                        <small>Orders delivered</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <!-- <span class="label label-primary float-right">Today</span> -->
                        <h5>Contact Queries</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ \App\Models\ContactQuery::all()->count() }}</h1>
                        <!-- <div class="stat-percent font-bold text-navy">44% <i class="fa fa-level-up"></i></div> -->
                        <small>Total contact queries</small>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="ibox ">
                    <div class="ibox-title">
                        <!-- <span class="label label-danger float-right">Low value</span> -->
                        <h5>Responded To Queries</h5>
                    </div>
                    <div class="ibox-content">
                        <h1 class="no-margins">{{ \App\Models\ContactQuery::where('admin_responded',1)->get()->count() }}</h1>
                        <!-- <div class="stat-percent font-bold text-danger">38% <i class="fa fa-level-down"></i></div> -->
                        <small>Total query response</small>
                    </div>
                </div>
    		</div>
        </div>
    </div>
    <!-- <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-sm-4">
            <h2>This is main title</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="index.html">This is</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Breadcrumb</strong>
                </li>
            </ol>
        </div>
        <div class="col-sm-8">
            <div class="title-action">
                <a href="" class="btn btn-primary">This is action area</a>
            </div>
        </div>
    </div>

    <div class="wrapper wrapper-content">
        <div class="middle-box text-center animated fadeInRightBig">
            <h3 class="font-bold">This is page content</h3>
            <div class="error-desc">
                You can create here any grid layout you want. And any variation layout you imagine:) Check out
                main dashboard and other site. It use many different layout.
                <br/><a href="{{ route('admin.dashboard') }}" class="btn btn-primary m-t">Dashboard</a>
            </div>
        </div>
    </div> -->
@endsection()