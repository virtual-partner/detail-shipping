<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>TPCL | Reset Password</title>

    <link href="{{ asset('admin_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <link href="{{ asset('admin_assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('admin_assets/css/style.css') }}" rel="stylesheet">

</head>

<body class="gray-bg">
    <div class="passwordBox animated fadeInDown">
        <div class="row">
            @if(session()->has('success'))
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Success
                        </strong>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Error !
                        </strong>
                        {{ session('error') }}
                    </div>
                </div>
            @endif
            <div class="col-md-12">
                <div class="ibox-content">

                    <h2 class="font-bold">Reset password</h2>

                    <p>
                        Enter your new password.
                    </p>
                        
                    <div class="col-lg-12">
                        <form class="m-t" role="form" method="POST" action="{{ route('admin.reset.password.form.submit') }}">
                            @csrf
                            <input type="hidden" name="reset_token" value="{{ $token }}">
                            <div class="form-group">
                                <input type="password" name="new_password" id="new_password" class="form-control" placeholder="New Password">
                                @if ($errors->has('new_password'))
                                    <span style="color: red;">{{ $errors->first('new_password') }}</span>
                                @endif
                            </div>
                            
                            <div class="form-group">
                                <input type="password" name="confirm_new_password" id="confirm_new_password" class="form-control" placeholder="Confirm New Password">
                                @if ($errors->has('confirm_new_password'))
                                    <span style="color: red;">{{ $errors->first('confirm_new_password') }}</span>
                                @endif
                            </div>

                            <button type="submit" class="btn btn-primary block full-width m-b">Reset password</button>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
        <div class="row">
            <div class="col-md-6">
                Copyright Example Company
            </div>
            <div class="col-md-6 text-right">
                <small>© 2014-2015</small>
            </div>
        </div>
    </div>
<!-- Mainly scripts -->
<script src="{{ asset('admin_assets/js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('admin_assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('admin_assets/js/bootstrap.js') }}"></script>

</body>

</html>