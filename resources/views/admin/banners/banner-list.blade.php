@extends('layouts.admin.main')
@section('title','Banner List')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Banner List</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Banners</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            @if(session()->has('success'))
                <div class="col-sm-12">
                    <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Success
                        </strong>
                        {{ session('success') }}
                    </div>
                </div>
            @endif
            @if(session()->has('error'))
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                            &times;
                        </button>
                        <strong>
                            Error !
                        </strong>
                        {{ session('error') }}
                    </div>
                </div>
            @endif
            <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Banner list with data</h5>
                    <div class="ibox-tools">
                        <button type="button" href="javascript:void()" data-target="#add-banner-image-modal" data-toggle="modal" class="btn btn-w-m btn-primary" style="margin-top: -7px;">
                            Add New Banner
                        </button>
                    </div>
                </div>
                <div class="ibox-content">

                    <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover dataTables-example" >
                <thead>
                <tr>
                    <th>Sr. No.</th>
                    <th>Banner Image </th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($bannerImages as $key => $bannerImage)
                        <tr style="text-align: center;" class="gradeX">
                            <td>
                                {{ $key+1 }}
                            </td>
                            <td>
                                <img src="{{ $bannerImage->banner_image }}" height="200px" width="200px" alt="">
                            </td>
                            <td>
                                @if ($bannerImage->is_active)
                                    <span class="badge badge-primary">Active</span>
                                @else
                                    <span class="badge badge-danger">Inactive</span>
                                @endif
                            </td>
                            <td>
                                <!-- &nbsp;|&nbsp; -->
                                <a href="javascript:void()" title="Delete" onclick="deleteBannerImage('{{ route('admin.banners.destroy',['banner_id'=>encrypt($bannerImage->id)]) }}')">
                                    <i class="fa fa-trash fa-2x" style="color:red;"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                <tr>
                    <th>Sr. No.</th>
                    <th>Banner Image </th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
                </tfoot>
                </table>
                    </div>

                </div>
            </div>
        </div>
        </div>
    </div>
    
    <div class="modal inmodal fade" id="add-banner-image-modal" tabindex="-1" role="dialog"  aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title">Add Banner</h4>
                    <!-- <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small> -->
                </div>
                <div class="modal-body">
                    <form id="add-banner-image-form" method="POST" action="{{ route('admin.banners.add') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group  row">
                            <label class="col-sm-4 col-form-label">Banner Image :</label>
                            <div class="col-sm-8">
                                <input type="file" name="banner_image" id="banner_image" class="form-control">
                            </div>
                        </div>
                    </div>
                    
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

<script src="{{ asset('admin_assets/js/plugins/dataTables/datatables.min.js') }}"></script>
<script src="{{ asset('admin_assets/js/plugins/dataTables/dataTables.bootstrap4.min.js') }}"></script>

<!-- Page-Level Scripts -->
<script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: []

            });

        });

        function deleteBannerImage(url) {
            var csrf_token  =   "{{ csrf_token() }}";
            swal({
                title: "Are you sure?",
                text: "You won't be able to recover the deleted blog!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        url  : url,
                        type : "DELETE",
                        data : {_token:csrf_token}
                    })
                    .done(function(response){
                        if (response.status) {
                            swal({
                                title : response.title,
                                text : response.message,
                                type : "success"
                            }, function(){
                                window.location.reload();
                            });
                        } else {
                            swal({
                                title : response.title,
                                text  : response.message,
                                type  : "error"
                            });
                        }
                    })
                    .fail(function(response){
                        console.log('fail');
                    })
                    .always(function(){
                        console.log('complete');
                    });
                }
            });
        }
    </script>
@endsection