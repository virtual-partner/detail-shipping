@extends('layouts.admin.main')
@section('title','Order List')
@section('content')
<link href="{{ asset('admin_assets/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>Custom Email</h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('admin.dashboard') }}">Dashboard</a>
                </li>
                <li class="breadcrumb-item active">
                    <strong>Custom Email Form</strong>
                </li>
            </ol>
        </div>
        <div class="col-lg-2">

        </div>
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
                @if(session()->has('success'))
                    <div class="col-sm-12">
                        <div class="alert alert-success">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            <strong>
                                Success
                            </strong>
                            {{ session('success') }}
                        </div>
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            <strong>
                                Error !
                            </strong>
                            {{ session('error') }}
                        </div>
                    </div>
                @endif
                @if (count($errors) > 0)
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                                &times;
                            </button>
                            <h4><i class="icon fa fa-ban"></i> Error!</h4>
                            @foreach($errors->all() as $error)
                            {{ $error }} <br>
                            @endforeach  
                        </div>
                    </div>
                    
                @endif
                <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-title">
                        <h5>Order list with data</h5>
                        <div class="ibox-tools">
                        
                        </div>
                    </div>
                    <div class="ibox-content">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-12">
                                    <form id="add-order-data-form" action="{{ route('admin.send.custom.email.notification') }}" method="POST">
                                        @csrf
                                        <div class="form-group  row">
                                            <label class="col-sm-4 col-form-label">Name :</label>
                                            <div class="col-sm-8">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Name">
                                                @if ($errors->has('name'))
                                                    <span style="color: red;">{{ $errors->first('name') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group  row">
                                            <label class="col-sm-4 col-form-label">Email :</label>
                                            <div class="col-sm-8">
                                                <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                                                @if ($errors->has('email'))
                                                    <span style="color: red;">{{ $errors->first('email') }}</span>
                                                @endif
                                            </div>

                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group  row">
                                            <label class="col-sm-4 col-form-label">Email :</label>
                                            <div class="col-sm-8">
                                                <textarea class="form-control" name="message" id="message" cols="30" rows="10">{{ old('message') }}</textarea>
                                                @if ($errors->has('message'))
                                                    <span style="color: red;">{{ $errors->first('message') }}</span>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="hr-line-dashed"></div>
                                        <div class="form-group row pull-right">
                                            <div class="col-12">
                                                <input class="btn btn-success pull-right" type="submit" value="Send">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection