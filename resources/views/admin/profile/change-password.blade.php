@extends('layouts.admin.main')
@section('title','Update Password')
@section('content')

<style>
    .validation_error {
        color: red;
    }
</style>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Profile</h2>
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="{{ route('admin.dashboard') }}">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">
                <strong>Change Password</strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">

    </div>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        @if(session()->has('success'))
            <div class="col-sm-12">
                <div class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <strong>
                        Success
                    </strong>
                    {{ session('success') }}
                </div>
            </div>
        @endif
        @if(session()->has('error'))
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;
                    </button>
                    <strong>
                        Error !
                    </strong>
                    {{ session('error') }}
                </div>
            </div>
        @endif
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Change Password</h5>
                </div>
                <div class="ibox-content">
                    <form action="{{ route('admin.update.password.form.submit') }}" method="POST">
                        @csrf
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">Current Password</label>
                            <div class="col-lg-10">
                                <input type="password" placeholder="Password" name="current_password" class="form-control">
                                @if ($errors->has('current_password'))
                                    <span class="validation_error">{{ $errors->first('current_password') }}</span>
                                @endif
                            </div>
                        </div>
                       
                        <div class="form-group row">
                            <label class="col-lg-2 col-form-label">New Password</label>
                            <div class="col-lg-10">
                                <input type="password" placeholder="New Password" name="new_password" class="form-control">
                                @if ($errors->has('new_password'))
                                    <span class="validation_error">{{ $errors->first('new_password') }}</span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group row"><label class="col-lg-2 col-form-label">Confirm New Password</label>

                            <div class="col-lg-10"><input type="password" placeholder="Confirm New Password" name="new_password_confirmation" class="form-control"></div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-lg-12" style="text-align: right;">
                                <button class="btn btn-sm btn-primary" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection