@extends('layouts.website.main')
@section('title','Trucking Freight')
@section('content')

<!--  start  about  -->
<div class="banner-inner-page" style="background: url(../assets/images/freight-banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>TRUCKING FREIGHT SERVICES</h2>
				<p>Offering the best in ground transportation, our Trucking Services put all of your shipping and transport needs into high gear. TPCL offers licensed and reliable transport from the first mile to the final destination, from heavy machinery to fragile items.</p>
				<p>Our services include nationwide pickup and delivery, full-service consolidation and distribution, accurate and real-time online shipment tracking services, and port pickup and drop-offs for international cargo shipping. A variety of truck sizes are equipped to handle every need, from industrial-sized cargo to fresh produce and retail goods. Let our Trucking Services take you on the road to business fulfillment with various quality and cost-effective shipping solutions.</p>
			</div>
		</div>
	</div>
</div>


<!--  branches start  -->
<div class="service-bottom-box">
	<div class="container">
		<div class="row heading">
			<h2>TRUCKING SERVICES</h2>
		</div>
		
		<div class="row services-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
						<h2>Full Truck Load (FTL)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/air-freight.png') }}"/>
						<h2>Less Than a Truck Load ( LTL)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/rail.png') }}"/>
						<h2>Drayage of Intermodal Containers</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/trucking.png') }}"/>
						<h2>Final Mile</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Temperature Controlled</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Over Dimensional</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Heavy Haul</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Time Critical</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Relocations/ Personal Effects</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!--  branches start  -->
<div class="service-testimonial">
	<div class="container">
		<div class="row heading">
			<div class="text">
				<p> TPCL offers our customers a track and track program that provides real-time updates at each node of your shipment, including both the departure and arrival of your freight.</p>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection