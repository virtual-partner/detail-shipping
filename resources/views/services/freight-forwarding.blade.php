@extends('layouts.website.main')
@section('title','Freight Forwarding')
@section('content')
<!--  start  about  -->
<div class="banner-inner-page" style="background: url(../assets/images/freight-banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>Freight forwarding</h2>
				<p>In the 21st century, your products and services need to keep up with the speed of your business ideas. TPCL’s proven best-in-class international and domestic freight forwarding services enable companies to remain ahead of the competition getting product to market quickly and efficiently.</p>
				<p>TPCL’s direct contracts with major alliances, advanced information systems, and logistics experts’ team provides customers with access to competitive rates, end-to-end visibility, and reliable information, resulting in an efficient and effective supply chain.</p>
				<p><b>Power in Numbers</b></p>
				<p>Through a robust network of global freight forwarders and combined scale of buying power, TPCL’s delivers access to consistent space and rates regardless of the lane or mode of transportation, reducing variability at every step.</p>
			</div>
		</div>
	</div>
</div>

<!--  branches start  -->
<div class="service-bottom-box">
	<div class="container">
		<div class="row heading">
			<h2>Freight Forwarding Services</h2>
		</div>
		
		<div class="row services-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
						<h2>Ocean Freight</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/air-freight.png') }}"/>
						<h2>Air Freight</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/rail.png') }}"/>
						<h2>Rail</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/trucking.png') }}"/>
						<h2>Domestic Trucking</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Warehousing & Distribution</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!--  branches start  -->
<div class="service-testimonial">
	<div class="container">
		<div class="row heading">
			<div class="text">
				<p>At TPCL we offer free track and trace technology to all of our customers.During the onboarding process a Web Tracker account, customized with your preferred milestones will be created. Upon receiving your unique credentials, you can log-in to our web tracker platform day or night to review the status of your shipment.</p>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection