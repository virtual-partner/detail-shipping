@extends('layouts.website.main')
@section('title','Air Freight')
@section('content')

<!--  start  about  -->
<div class="banner-inner-page" style="background: url(../assets/images/freight-banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>AIR FREIGHT</h2>
				<p>When your cargo needs to move quickly, and with accuracy, Air Freight is the preferred mode of transportation. <b>TRANSPACIFIC CARGO AND LOGISTICS</b> vast network provides far-reaching coverage of all major airports allowing for flexible routing, visibility, and secure end-to-end service. Regardless of the commodity, TPCL’s Air Freight specialist team will provide customized solutions to maximize efficiency and ensure your goods’ on-time delivery.</p>
			</div>
		</div>
	</div>
</div>


<!--  branches start  -->
<div class="service-bottom-box">
	<div class="container">
		<div class="row heading">
			<h2>Air Freight Services</h2>
		</div>
		
		<div class="row services-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
						<h2>Time Critical</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/air-freight.png') }}"/>
						<h2>Next Flight Out (NFO)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/rail.png') }}"/>
						<h2>Priority</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/trucking.png') }}"/>
						<h2>Hand Courier</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Charters</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Overweight</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Hazmat</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>All-Risk Full Coverage Insurance</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!--  branches start  -->
<div class="service-testimonial">
	<div class="container">
		<div class="row heading">
			<div class="text">
				<p> Yes, TPCL offers All-Risk Cargo insurance that ensures your goods are protected from origin to destination.</p>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection