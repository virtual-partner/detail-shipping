@extends('layouts.website.main')
@section('title','Warehouse And Distribution')
@section('content')

<!--  start  about  -->
<div class="banner-inner-page" style="background: url(../assets/images/freight-banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>WAREHOUSE AND DISTRIBUTION</h2>
				<p>When it comes to warehousing, distribution, and storage services, TPCL’s state-of-the-art facilities ensure your products are safe, secure, and ready for shipment.From the end of the production line to the hands of your customers,TPCL’s distribution services offer secure, reliable, and trusted transport.</p>
				<p>With offices and warehouses located in Chicago, Illinois, Miami, Florida, Charleston, South Carolina, and Los Angeles, California, TPCL’s qualified and experienced staff are experts in working with various manufacturers, importers and exporters, wholesalers, and transport businesses. TPCL’s wide range of warehousing and distribution services offers comprehensive solutions with national coverage. Let us offer you the best in Warehousing & Distribution Services to ensure your products are secure in place and safe on arrival.</p>
				<p>Yes, all of TPCL’s U.S. warehouses are fully insured. Protecting your goods is our top priority.</p>
			</div>
		</div>
	</div>
</div>


<!--  branches start  -->
<div class="service-bottom-box">
	<div class="container">
		<div class="row heading">
			<h2>WAREHOUSE & DISTRIBUTION SERVICES</h2>
		</div>
		
		<div class="row services-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
						<h2>Online WMS System with Real-Time Inventory Updates</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/air-freight.png') }}"/>
						<h2>Cross Docking</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/rail.png') }}"/>
						<h2>Stripping, Stuffing & Segregation</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/trucking.png') }}"/>
						<h2>Pick and Pack</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Domestic Distribution & Fulfillment</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Inventory Management</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Crating / Palletization Services</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Consolidations</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Daily / Weekly / Monthly Storage</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Complete Supply Chain Management</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Personal Effects / Household Goods</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Automotive Blocking / Bracing</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!--  branches start  -->
<div class="service-testimonial">
	<div class="container">
		<div class="row heading">
			<div class="text">
				<p>Have questions about our Warehousing & Distribution Services? For more information on our nationwide warehousing and distribution service or to obtain a quote please contact us at <b>info@transpacificinternational.com</b> or fill out the below form and one of our representatives will be in touch shortly.</p>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection