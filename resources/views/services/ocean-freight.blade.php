@extends('layouts.website.main')
@section('title','Ocean Freight')
@section('content')

<!--  start  about  -->
<div class="banner-inner-page" style="background: url(../assets/images/freight-banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>OCEAN FREIGHT</h2>
				<p>Cost-effective and reliable, Ocean Freight continues to be the most utilized mode of transportation in the world. TPCL’s Ocean Transportation department will help you navigate the best routing and service for your freight. Our vast portfolio of solutions allows TPCL’s team of  experts to securely and efficiently transport your goods to their final destination.</p>
				<p>As an NVOCC ( non-vessel operating common carrier),TPCL has secured strong contract rates with steamship lines worldwide. These rates in addition to TPCL’s global network of partner agents, means that TPCL can provide you with the competitive rates and end-to-end visibility you require.</p>
			</div>
		</div>
	</div>
</div>


<!--  branches start  -->
<div class="service-bottom-box">
	<div class="container">
		<div class="row heading">
			<h2>OCEAN FREIGHT SERVICES</h2>
		</div>
		
		<div class="row services-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
						<h2>Full container load (FCL)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/air-freight.png') }}"/>
						<h2>Consolidation/less than container load (LCL)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/rail.png') }}"/>
						<h2>Break Bulk</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/trucking.png') }}"/>
						<h2>Temperature Controlled</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Lift-on/Lift-off (LoLo)</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/service-warehouse.png') }}"/>
						<h2>Roll-on/Roll-off (RoRo)</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!--  branches start  -->
<div class="service-testimonial">
	<div class="container">
		<div class="row heading">
			<div class="text">
				<p> TPCL offers our customers a track and track program that provides real-time updates at each node of your shipment, including both the departure and arrival of your freight.</p>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection