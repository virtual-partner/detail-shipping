@extends('layouts.website.main')
@section('title','About Us')
@section('content')
<!--  start  about  -->
<div class="banner-inner-page" style="background: url(assets/images/about_banner.png);">
	<div class="container">
		<div class="row">
			
		</div>
	</div>
</div>
<!--  end  about  -->

<div class="about-page-block">
	<div class="container">
		<div class="row">
			<div class="text">
				<h2>About TPCL</h2>
				<p><b>TRANSPACIFIC CARGO AND LOGISTICS</b> mission is to facilitate our clients’ growth and development by providing efficient and effective supply chain partnerships with an emphasis on quality, performance, and integrity. Our headquarters in Los Angeles, California positions us next to the largest port in the United States. We have branch offices in Chicago, Illinois, Miami, Florida, Charleston, South Carolina and Mexico, Bogota, Colombia.</p>
				<p>Our vision is to establish a culture of equity, opportunity, and growth amongst all stakeholders, where open and honest communication guides the development of individuals and partnerships, allowing them to reach their full potential. To expand our global footprint through our mission of uncompromising quality, performance, and integrity, increasing our scope and capabilities while remaining dedicated to our corporate values.</p>
				<p>Through an emphasis on customer service to our clients as well as our valued worldwide partners, we have been rewarded with extensive support and growth.</p>
				<p>It is our dedicated and custom-built service that separates us from our competitors.</p>
				<p>We value a sense of urgency and impeccable execution in the services we provide.</p>
				<p>We encourage a “Speak Up, Listen, and Take Action” culture amongst our teams, and embody a “No Excuses” mentality toward success.</p>
			</div>
		</div>
	</div>
</div>


<!--  branches start  -->
<div class="about-our-services">
	<div class="container">
		<div class="row heading">
			<h2>Our services</h2>
		</div>
		
		<div class="row about-services">
			<div class="col-md-4 item-single">
				<a href="{{ route('services.freight.forwarding') }}">
					<div class="item">
						<div class="image">
							<img src="{{ asset('assets/images/freight.png') }}"/>
							<h2>FREIGHT FORWARDING</h2>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4 item-single">
				<a href="{{ route('services.freight.ocean') }}">
					<div class="item">
						<div class="image">
							<img src="{{ asset('assets/images/ocean-freight.png') }}"/>
							<h2>OCEAN FREIGHT</h2>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4 item-single">
				<a href="{{ route('services.freight.air') }}">
					<div class="item">
						<div class="image">
							<img src="{{ asset('assets/images/air-freight.png') }}"/>
							<h2>AIR FREIGHT</h2>
						</div>
					</div>
				</a>
			</div>
		</div>
		
		<div class="row about-services">
			<div class="col-md-4 item-single">
				<a href="{{ route('services.freight.trucking') }}">
					<div class="item">
						<div class="image">
							<img src="{{ asset('assets/images/trucking.png') }}"/>
							<h2>TRUCKING FREIGHT SERVICES</h2>
						</div>
					</div>
				</a>
			</div>
			<div class="col-md-4 item-single">
				<a href="{{ route('services.warehouse.distribution') }}">
					<div class="item">
						<div class="image">
							<img src="{{ asset('assets/images/warehouse.png') }}"/>
							<h2>WAREHOUSE AND DISTRIBUTION</h2>
						</div>
					</div>
				</a>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->
@endsection