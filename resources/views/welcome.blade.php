@extends('layouts.website.main')
@section('title','Home')
@section('content')
<!--  banner start  --> 
<div class="bannersliderBox owl-carousel" id="bannerslider" >
	<div class="item" style="background: url({{ asset('assets/images/banner.png') }}) no-repeat;">
		<div class="container">
			<div class="row">
				<div class="text">
					<h2>Your Trusted Delivery Partner </h2>					
					<p><b>TRANSPACIFIC CARGO AND LOGISTICS (TPCL)</b>  mission is to connect companies on an international scale through information and supply chain management that provides success and maximized revenues. Our headquarters in Los Angeles, California positions us next to the largest port in the United States. We have branch offices in Chicago, Illinois, Miami, Florida, as well as Mexico,Bogota, Colombia. Our newest office is now open in Charleston, South Carolina.</p>
					<!-- <div class="banner-form">
						<span>Track Your Order</span>			
						<form class="banner-form-inner">
							<input type="text" class="form-control" placeholder="Enter Here" />
							<input type="button" name="Track Order" placeholder="Track Order" value="Track Order" />
						</form>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="item" style="background: url({{ asset('assets/images/banner2.jpg') }}) no-repeat;">
		<div class="container">
			<div class="row">
				<div class="text">
					<h2>Your Trusted Delivery Partner </h2>					
					<p>Through an emphasis on customer service to our clients as well as our valued worldwide partners, we have been rewarded with extensive support and growth. It is our dedicated and custom built service that separates us from our competitors. We value a sense of urgency and impeccable execution in the services we provide. We encourage a “Speak Up, Listen, and Take Action” culture amongst our teams, and embody a “No Excuses” mentality toward success.</p>
					<!-- <div class="banner-form">
						<span>Track Your Order</span>			
						<form class="banner-form-inner">
							<input type="text" class="form-control" placeholder="Enter Here" />
							<input type="button" name="Track Order" placeholder="Track Order" value="Track Order" />
						</form>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<div class="item" style="background: url({{ asset('assets/images/banner3.jpg') }}) no-repeat;">
		<div class="container">
			<div class="row">
				<div class="text">
					<h2>Your Trusted Delivery Partner </h2>					
					<p><b>TRANSPACIFIC CARGO AND LOGISTICS (TPCL)</b>  is guided by our Three Pillars of Success: Attitude, Effort, and Self-Improvement. We believe that when our clients and partners win, we win, and our Three Pillars grant us an engaged and performance-based culture with an emphasis on efficiency, productivity, agility, and reliability.</p>
					<!-- <div class="banner-form">
						<span>Track Your Order</span>			
						<form class="banner-form-inner">
							<input type="text" class="form-control" placeholder="Enter Here" />
							<input type="button" name="Track Order" placeholder="Track Order" value="Track Order" />
						</form>
					</div> -->
				</div>
			</div>
		</div>
	</div>
	<!-- @foreach ($banners as $banner)
		<div class="item" style="background: url({{$banner->banner_image}}) no-repeat;">
			<div class="container">
				<div class="row">
					<div class="text">
						<h2>Your Trusted Delivery Partner </h2>					
						<p><b>TRANSPACIFIC CARGO AND LOGISTICS (TPCL)</b>  mission is to connect companies on an international scale through information and supply chain management that provides success and maximized revenues. Our headquarters in Los Angeles, California positions us next to the largest port in the United States. We have branch offices in Chicago, Illinois, Miami, Florida, as well as Mexico,Bogota, Colombia. Our newest office is now open in Charleston, South Carolina.</p>
						<p>Through an emphasis on customer service to our clients as well as our valued worldwide partners, we have been rewarded with extensive support and growth. It is our dedicated and custom built service that separates us from our competitors. We value a sense of urgency and impeccable execution in the services we provide. We encourage a “Speak Up, Listen, and Take Action” culture amongst our teams, and embody a “No Excuses” mentality toward success.</p>
						<p><b>TRANSPACIFIC CARGO AND LOGISTICS (TPCL)</b>  is guided by our Three Pillars of Success: Attitude, Effort, and Self-Improvement. We believe that when our clients and partners win, we win, and our Three Pillars grant us an engaged and performance-based culture with an emphasis on efficiency, productivity, agility, and reliability.</p>
						<div class="banner-form">
							<span>Track Your Order</span>			
							<form class="banner-form-inner">
								<input type="text" class="form-control" id="tracking_number" placeholder="Enter Here" />
								<input type="button" name="Track Order" onclick="getOrderTrackingDetails()" placeholder="Track Order" value="Track Order" />
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	@endforeach -->
	
</div>
<!--  banner end  -->

<!-- Track Order Form -->
<div class="banner-bottom-form">
	<div class="banner-form">
		<span>Track Your Order</span>			
		<form class="banner-form-inner">
			<input type="text" class="form-control" id="tracking_number" placeholder="Enter Tracking Number Here" />
			<input type="button" name="Track Order" onclick="getOrderTrackingDetails()" placeholder="Track Order" value="Track Order" />
		</form>
	</div>
</div>

<!--  banner bottom start  -->
<div class="our-icons">
	<div class="row icons-main">
		<div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" style="background: #3C93E0;">
			<div class="item">
				<div class="image">
					<img class="img-responsive" src="{{ asset('assets/images/noun_Logistics.png') }}" alt="">
				</div>
				<div class="text">
					<h3>Fast Nationwide Delivery</h3>
					<p>facilitate our clients’ growth and development by.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" style="background: #2170BB;">
			<div class="item">
				<div class="image">
					<img class="img-responsive" src="{{ asset('assets/images/noun_throughout.png') }}" alt="">
				</div>
				<div class="text">
					<h3>End-To-End Solution Available</h3>
					<p>facilitate our clients’ growth and development by.</p>
				</div>
			</div>
		</div>
		<div class="col-sm-4 col-xs-12 col-md-4 col-lg-4" style="background: #145292;">
			<div class="item">
				<div class="image">
					<img class="img-responsive" src="{{ asset('assets/images/safety.png') }}" alt="">
				</div>
				<div class="text">
					<h3>Safety & Compliance</h3>
					<p>facilitate our clients’ growth and development by.</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--  banner bottom end  -->
	
<!--  start  about  --> 
<div class="aboutBox">
	<div class="container">
		<div class="row">
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 text">
				<h2>About TPCL</h2>
				<p><b>TRANSPACIFIC CARGO AND LOGISTICS</b> mission is to facilitate our clients’ growth and development by providing efficient and effective supply chain partnerships with an emphasis on quality, performance, and integrity.</p>
				<p>Our headquarters in Los Angeles, California positions us next to the largest port in the United States.</p>
				<p>We have branch offices in Chicago, Illinois, Miami, Florida, Charleston, South Carolina and Mexico, Bogota, Colombia.</p>					
				<p><a href="{{ route('about-us') }}" class="viewfulldetails">Know More</a></p>
			</div>
			<div class="col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 image">
				<img src="{{ asset('assets/images/about-image.png') }}" alt="alt" class="img-responsive"/>
			</div>
		</div>
	</div>
</div>
<!--  end  about  -->

<!--  services start  -->
<div class="services-box">
	<div class="container">
		<div class="row heading">
			<h2>Our services</h2>
			<p>California positions us next to the largest port in the United States.</p>
		</div>
		
		<div class="row service-item-one">
			<div class="col-md-6">
				<div class="image">
					<img src="{{ asset('assets/images/freight.png') }}" alt="alt" class="img-responsive"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-box">
					<h2>Freight Forwarding</h2>
					<p>In the 21st century, your products and services need to keep up with the speed of your business ideas. TPCL’s proven best-in-class international and domestic freight forwarding services enable companies to remain ahead of the competition getting product to market quickly and efficiently.</p>
					<p>TPCL’s direct contracts with major alliances, advanced information systems, and logistics experts’ team provides customers with access to competitive rates, end-to-end visibility, and reliable information, resulting in an efficient and effective supply chain.</p>
					<p><a href="{{ route('services.freight.forwarding') }}" class="viewfulldetails">Know More</a></p>
				</div>
			</div>
		</div>
		
		<div class="row service-item-one reverse-order">
			<div class="col-md-6">
				<div class="text-box">
					<h2>OCEAN FREIGHT</h2>
					<p>Cost-effective and reliable, Ocean Freight continues to be the most utilized mode of transportation in the world. TPCL’s Ocean Transportation department will help you navigate the best routing and service for your freight. Our vast portfolio of solutions allows TPCL’s team of experts to securely and efficiently transport your goods to their final destination. As an NVOCC ( non-vessel operating common carrier),TPCL has secured strong contract rates with steamship…</p>
					<p><a href="{{ route('services.freight.ocean') }}" class="viewfulldetails">Know More</a></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="image">
					<img src="{{ asset('assets/images/ocean-freight.png') }}" alt="alt" class="img-responsive"/>
				</div>
			</div>
		</div>
		
		<div class="row service-item-one">
			<div class="col-md-6">
				<div class="image">
					<img src="{{ asset('assets/images/air-freight.png') }}" alt="alt" class="img-responsive"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-box">
					<h2>AIR FREIGHT</h2>
					<p>When your cargo needs to move quickly, and with accuracy, Air Freight is the preferred mode of transportation. <b>TRANSPACIFIC CARGO AND LOGISTICS</b> vast network provides far-reaching coverage of all major airports allowing for flexible routing, visibility, and secure end-to-end service. Regardless of the commodity, TPCL’s Air Freight specialist team will provide customized solutions to maximize efficiency and ensure your goods’ on-time delivery.</p>
					<p><a href="{{ route('services.freight.air') }}" class="viewfulldetails">Know More</a></p>
				</div>
			</div>
		</div>
		
		<div class="row service-item-one reverse-order">				
			<div class="col-md-6">
				<div class="text-box">
					<h2>TRUCKING FREIGHT SERVICES</h2>
					<p>Offering the best in ground transportation, our Trucking Services put all of your shipping and transport needs into high gear. TPCL offers licensed and reliable transport from the first mile to the final destination, from heavy machinery to fragile items. Our services include nationwide pickup and delivery, full-service consolidation and distribution, accurate and real-time online shipment tracking services, and port pickup and drop-offs for international cargo shipping. A variety of truck sizes are equipped to handle every need, from industrial-sized cargo to fresh produce and retail goods.
Let our Trucking Services take you on the road to business fulfillment with various quality and cost-effective shipping solutions.</p>
					<p><a href="{{ route('services.freight.trucking') }}" class="viewfulldetails">Know More</a></p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="image">
					<img src="{{ asset('assets/images/trucking.png') }}" alt="alt" class="img-responsive"/>
				</div>
			</div>
		</div>
		
		<div class="row service-item-one">
			<div class="col-md-6">
				<div class="image">
					<img src="{{ asset('assets/images/warehouse.png') }}" alt="alt" class="img-responsive"/>
				</div>
			</div>
			<div class="col-md-6">
				<div class="text-box">
					<h2>WAREHOUSE AND DISTRIBUTION</h2>
					<p>When your cargo needs to move quickly, and with accuracy, Air Freight is the preferred mode of transportation. <b>TRANSPACIFIC CARGO AND LOGISTICS</b> vast network provides far-reaching coverage of all major airports allowing for flexible routing, visibility, and secure end-to-end service. Regardless of the commodity, TPCL’s Air Freight specialist team will provide customized solutions to maximize efficiency and ensure your goods’ on-time delivery.</p>
					<p><a href="{{ route('services.warehouse.distribution') }}" class="viewfulldetails">Know More</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
<!--  services end  -->


<!--  branches start  -->
<div class="branches-box">
	<div class="container">
		<div class="row heading">
			<h2>Our Branches</h2>
		</div>
		
		<div class="row branches-block">
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/california.png') }}"/>
						<h2>California</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/chicago.png') }}"/>
						<h2>Chicago</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/illinois.png') }}"/>
						<h2>Illinois</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/miami.png') }}"/>
						<h2>Miami</h2>
					</div>
				</div>
			</div>
			<div class="item-single">
				<div class="col-md-12 item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/florida.png') }}"/>
						<h2>Florida</h2>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row branches-block">
			<div class="col-md-3 item-single">
				<div class="item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/mexico.png') }}"/>
						<h2>Mexico</h2>
					</div>
				</div>
			</div>
			<div class="col-md-3 item-single">
				<div class="item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/bogota.png') }}"/>
						<h2>Bogota</h2>
					</div>
				</div>
			</div>
			<div class="col-md-3 item-single">
				<div class="item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/columbia.png') }}"/>
						<h2>Columbia</h2>
					</div>
				</div>
			</div>
			<div class="col-md-3 item-single">
				<div class="item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/charleston.png') }}"/>
						<h2>Charleston</h2>
					</div>
				</div>
			</div>
			<div class="col-md-3 item-single">
				<div class="item">
					<div class="image">
						<img src="{{ asset('assets/images/branches/los-angles.png') }}"/>
						<h2>Los Angles</h2>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
<!--  branches end  -->

<!-- Order Status Tracking Modal -->
<div class="modal fade" id="order-tracking-details-modal"></div>

<script>
	function getOrderTrackingDetails() {
		var tracking_number 	=	$("#tracking_number").val();
		$.ajax({
                url  : "{{ route('order.tracking.details') }}",
                type : "GET",
                data : {tracking_number:tracking_number}
            })
            .done(function(response){
				$("#order-tracking-details-modal").html(response);
				$("#order-tracking-details-modal").modal('toggle');
            })
            .fail(function(response){
                $.each(response.responseJSON.errors,function(index,value){
                    alert(value);
                });
            })
            .always(function(){

            });
	}

</script>
@endsection