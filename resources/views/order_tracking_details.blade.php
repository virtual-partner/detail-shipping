<div class="modal-dialog" style="max-width: 80%;">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Order Tracking Details</h4>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        </div>
        <div class="modal-body">
            <div class="col-12">
                <div class="row">
                    <div class="col-4">
                        <h5 for="">Tracking Number :</h5>
                        <span><b>{{ $orderData->item_tracking_number }}</b></span>
                    </div>
                    <div class="col-4">
                        <h5 for="">Order Date :</h5>
                        <span><b>{{ date('d M Y H:i:s A', strtotime($orderData->created_at)) }}</b></span>
                    </div>
                    <div class="col-4">
                        <h5 for="">Shipping Address :</h5>
                        <span><b>{{ $orderData->shipping_address }}</b></span>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-4">
                        <h5 for="">Item Name :</h5>
                        <span><b>{{ $orderData->item_name }}</b></span>
                    </div>
                    <div class="col-4">
                        <h5 for="">Item Weight :</h5>
                        <span><b>{{ $orderData->item_weight }}</b></span>
                    </div>
                    <div class="col-4">
                        <h5 for="">Package Details :</h5>
                        <span><b>{{ $orderData->address_from }}</b></span>
                    </div>
                </div>
                <!-- <br> -->
                <hr>
                <div class="row">
                    
                    <table class="table table-striped table-bordered table-hover dataTables-example" id="order-status-tracking-datatable">
                        <thead>
                            <tr>
                                <th>Sr. No.</th>
                                <th>Order Status</th>
                                <th>Date & Time</th>
                                <th>Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orderData->order_status as $key => $status)
                                <tr style="background-color: {{ $status->color }};">
                                    <td>
                                        {{ $key+1 }}
                                    </td>
                                    <td>
                                        <b>{{ strtoupper($status->order_status) }}</b>
                                    </td>
                                    <td>
                                        {{ date('d M Y H:i:s A',strtotime($status->created_at)) }}
                                    </td>
                                    <td>
                                        {{ $status->comment }}
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <hr>
            <div class="col-12">
                <div class="row">
                    <table class="table table-bordered">
                        
                    </table>
                </div>
            </div>
        </div>
        <div class="modal-footer">

        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
		$('.dataTables-example').DataTable({
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                // buttons: [
                //     {extend: 'copy'},
                //     {extend: 'csv'},
                //     {extend: 'excel', title: 'ExampleFile'},
                //     {extend: 'pdf', title: 'ExampleFile'},

                //     // {extend: 'print',
                //     //  customize: function (win){
                //     //         $(win.document.body).addClass('white-bg');
                //     //         $(win.document.body).css('font-size', '10px');

                //     //         $(win.document.body).find('table')
                //     //                 .addClass('compact')
                //     //                 .css('font-size', 'inherit');
                //     // }
                //     // }
                // ]

            });
	});
</script>