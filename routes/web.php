<?php

use Illuminate\Support\Facades\Route;

use \App\Http\Controllers\Admin\Auth\LoginController as AdminLoginController;
use \App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\Admin\Auth\ForgotPasswordController;
use App\Http\Controllers\Admin\Auth\ResetPasswordController;
use App\Http\Controllers\Admin\SubAdminController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //  Getting banner list
    $banners    =   \App\Models\Banner::where('is_active',1)->get();

    return view('welcome',compact(['banners']));
})->name('homepage');

Route::get('/about-us', function () {
    return view('about-us');
})->name('about-us');

Route::prefix('services/')->as('services.')->group(function () {
   
    Route::get('freight-forwarding', function () {
        return view('services.freight-forwarding');
    })->name('freight.forwarding');
    
    Route::get('ocean-freight', function () {
        return view('services.ocean-freight');
    })->name('freight.ocean');
    
    Route::get('air-freight', function () {
        return view('services.air-freight');
    })->name('freight.air');
    
    Route::get('trucking-freight', function () {
        return view('services.trucking-freight');
    })->name('freight.trucking');
    
    Route::get('warehouse-and-distribution', function () {
        return view('services.warehouse-distribution');
    })->name('warehouse.distribution');

});

// Route::get('/blogs', function () {
//     return view('blogs');
// })->name('blogs');

Route::get('/blogs/{slug?}',[HomeController::class,'blogDetails'])->name('blogs.details');

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';


//  Route to track order details
Route::get('order/tracking/details',[HomeController::class,'index'])->name('order.tracking.details');

//  Route to save contact query by user
Route::post('contact/query',[HomeController::class,'saveContactQuery'])->name('contact.query.save');

/*----------------------------------------------
 *  ADMIN Routes
 * ---------------------------------------------
 * */

Route::prefix('admin/')->as('admin.')->group(function(){

    Route::get('/',function(){
        return redirect()->route('admin.login.show');
    });

    //  Login Route
    Route::get('login',[AdminLoginController::class,'create'])->name('login.show');
    
    //  Login Post Route
    Route::post('login',[AdminLoginController::class,'store'])->name('login');
    
    //  Forgot Password Form Route
    Route::get('forgot/password',[ForgotPasswordController::class,'forgotPasswordForm'])->name('forgot.password');
    
    //  Forgot Password Form Submit and reset password link Route
    Route::post('forgot/password',[ForgotPasswordController::class,'sendResetPasswordLink'])->name('forgot.password.submit');
    
    //  Reset Password form Route
    Route::get('reset/password/{token}',[ResetPasswordController::class,'resetPasswordForm'])->name('reset.password.form');
    
    //  Reset Password form submit Route
    Route::post('reset/password',[ResetPasswordController::class,'resetPassword'])->name('reset.password.form.submit');

    /*--------------------------------------------------
     *  ADMIN Auth Routes
     *--------------------------------------------------
     */
    Route::middleware('auth')->group(function(){

        //  Logout Route
        Route::post('logout',[AdminLoginController::class,'destroy'])->name('logout');

        //  Dashboard Route
        Route::get('dashboard',[AdminController::class,'dashboard'])->name('dashboard');
        
        //  Update Password Form Route
        Route::get('update/password',[AdminController::class,'changePasswordForm'])->name('update.password.form');
        
        //  Update Password Form Submit Route
        Route::post('update/password',[AdminController::class,'submitChangePasswordForm'])->name('update.password.form.submit');

        //  Order Routes Group
        Route::prefix('order/')->as('order.')->group(function(){

            //  Order List
            Route::get('list/{type}',[AdminController::class,'orderList'])->name('list');
            
            //  Create Order
            Route::post('data',[AdminController::class,'createOrder'])->name('create');
            
            //  Import Order Data
            Route::post('data/import',[AdminController::class,'importOrderData'])->name('import.data');

            //  Order Status Data
            Route::get('status/data/{order_id}',[AdminController::class,'orderStatusData'])->name('status.data');

            //  Order Data to Edit
            Route::get('edit/{order_id}',[AdminController::class,'editOrderData'])->name('edit');
            
            //  Update Order Data
            Route::put('update/{order_id}',[AdminController::class,'updateOrderData'])->name('update');

            //  Update Order Status
            Route::put('status/update/{order_id}',[AdminController::class,'updateOrderStatus'])->name('update.status');

        });

        //  Banner List route
       //  Route::get('banners/list',[AdminController::class,'bannerList'])->name('banners.list');
        
        //  Add Banner route
        // Route::post('banners/add',[AdminController::class,'addBanner'])->name('banners.add');
        
        //  Delete Banner route
        // Route::delete('banners/delete/{banner_id}',[AdminController::class,'destroyBanner'])->name('banners.destroy');
        
        //  Contact Queries route
        Route::get('contact/queries',[AdminController::class,'contactQueries'])->name('contact.queries');
        
        //  Contact Query Admin Response route
        Route::post('contact/queries/response',[AdminController::class,'contactQueryResponse'])->name('contact.query.response');
        
        //  Resource route of Sub Admin
        Route::resource('sub/admin', SubAdminController::class)->names('sub.admin')->parameters([
            'admin' =>  'id'
        ]);
        
        //  Resource route of Sub Admin
        Route::resource('blogs', BlogController::class)->names('blogs')->parameters([
             'blog' =>  'id'
        ]);
        
        //  Activate, Deactivate, Delete Sub Admin route
        Route::get('sub/admin/activate/deactivate/delete/{action}/{data_id}',[AdminController::class,'activateDeactivateDeleteSubAdmin'])->name('sub.admin.activate.deactivate.delete');

        //  Custom Email Notification Form
        // Route::get('custom/email/notification',[AdminController::class,'customEmailNotificationForm'])->name('custom.email.notification');
        Route::get('custom/email/notification',function(){
            return view('admin.custom-email-notification');
        })->name('custom.email.notification');
        
        //  Custom Email Notification Form Submit
        Route::post('custom/email/notification',[AdminController::class,'sendCustomEmailNotification'])->name('send.custom.email.notification');
    });
});
